import numpy as np

def get_crswir(self):
    """
    compute the CRswir indice
    """

    num = self.get_interpolated_Sentinel2_B11()

    den = self.get_interpolated_Sentinel2_B8A() + (1610 - 865) * (
                (self.get_interpolated_Sentinel2_B12() - self.get_interpolated_Sentinel2_B8A()) / (2190 - 865))
    den = np.where(den == 0, 1, den)

    coef = num / den

    labels = [f"CRswir_{i+1}" for i in range(coef.shape[2])]

    return coef, labels


def get_crre(self):
    """
    compute the CRre indice
    """
    # coef = self.get_interpolated_Sentinel2_B5() / (
    #         1.1 + self.get_interpolated_Sentinel2_B4() + (704 - 665) * (
    #         (self.get_interpolated_Sentinel2_B6() - self.get_interpolated_Sentinel2_B4()) / (741 - 665)))

    num = self.get_interpolated_Sentinel2_B5()

    den = self.get_interpolated_Sentinel2_B4() + (704 - 665) * (
            (self.get_interpolated_Sentinel2_B6() - self.get_interpolated_Sentinel2_B4()) / (741 - 665))
    den = np.where(den == 0, 1, den)

    coef = num / den

    labels = [f"CRre_{i + 1}" for i in range(coef.shape[2])]
    return coef, labels
