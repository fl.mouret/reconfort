# RECONFORT package

####  Note: a user guide (in french) is available here: [docs/Fiche_utilisateur_carte_Reconfort.pdf](/docs/Fiche_utilisateur_carte_Reconfort.pdf)

Oak dieback mapping using Sentinel 2 image and supervised classification.

Based on the work described in https://doi.org/10.1109/JSTARS.2023.3332420

This work was supported by the University of Orleans, CESBIO laboratory, University of Toulouse III Paul Sabatier within the
SYCOMORE program, with the financial support of the Région Centre-Val de Loire (France).


### Cite:

F. Mouret, D. Morin, H. Martin, M. Planells and C. Vincent-Barbaroux, "Toward an Operational Monitoring of Oak Dieback With Multispectral Satellite Time Series: A Case Study in Centre-Val De Loire Region of France," in IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing, doi: 10.1109/JSTARS.2023.3332420.
```
@ARTICLE{mouret_2023,
  author={Mouret, Florian and Morin, David and Martin, Hilaire and Planells, Milena and Vincent-Barbaroux, Cécile},
  journal={IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing}, 
  title={Toward an Operational Monitoring of Oak Dieback With Multispectral Satellite Time Series: A Case Study in Centre-Val De Loire Region of France}, 
  year={2023},
  volume={},
  number={},
  pages={1-18},
  doi={10.1109/JSTARS.2023.3332420}}
```

## Getting started : installation of your conda env. and iota2

#### The detailed procedure for installing iota2 (with additional details) is available here: https://docs.iota2.net/develop/HowToGetIOTA2.html 

#### Summary of the main installation steps:

- Step 1: download and install Miniconda
- Step 2: add iota2 channel
- Step 3: get the iota2 package and install it

Note on step 3 : it seems that ```-c iota2-deps``` is now needed to install iota2 properly:
```
conda create -n my_new_i2_env
conda activate my_new_i2_env
conda install python=3.9 mamba
mamba install iota2 -c iota2 -c iota2-deps
```

### Detailed installation procedure and Windows users installation: 

#### A more detailed tutorial on how to install the package is available here : [docs/tutorial.pdf](/docs/tutorial.pdf)

## Run main reconfort package scripts

After having installed your conda env, don't forget to activate it before running the reconfort package:

``` conda activate my_new_i2_env ```

### 1) run_download_S2_images

This script uses the theia_download package (https://github.com/olivierhagolle/theia_download) to download S2 image.

```
python run_download_S2_images.py -config_file config_file_theia_download_T31UEQ.cfg 
```

#### A Config file is needed, see example :

```
tile='T31UEQ' # tile to be downloaded
path_to_cfg_theia_account='/media/florian/data/theia_acces/config_theia.cfg' # theia account information
start='2021-01-01'  # Start date
end='2022-12-15'  # End date
zip_path='/media/florian/data/data_S2/zip/2022/T31UEQ' # path to download the data
out_dir='/media/florian/data/data_S2/2022/T31UEQ' # path to download the data
```

#### Note: The zip files are first downloaded to a specific folder. Then they are extracted to a new folder. The main reason for having two folders is that iota2 won't work if you mix .zip files and folders in the S2 data folder. Once the zip files are extracted, you can delete them to save space.

#### Note 2: you need an account @ theia (https://theia.cnes.fr/atdistrib/rocket/#/search?collection=SENTINEL2). The theia_download package need another cfg file with your account informaton (login and password):

path_to_cfg_theia_account.cfg :
```
serveur = https://theia.cnes.fr/atdistrib
resto = resto2
token_type = text
login_theia = your_login@email.fr
password_theia = your_password
```

#### Note 3: The download may stop after some time (due to security restrictions by theia).The download may stop after some time (due to security restrictions by theia). If this happens, simply reload the script. WARNING: Be sure to delete all .tmp files! (if you don't, the images with .tmp will not be downloaded)

### 2) run_process_downloaded_images (unzip downloaded files)

This script uses extract the .zip files downloaded using the previous script.

```
python run_process_downloaded_images.py -config_file config_file_theia_download_T31UEQ.cfg 
```


### 3) run_map_production_reconfort

```
python run_map_production_reconfort.py -config_file config_file_map_production_T31UEQ.cfg  
```

#### Another Config file is needed, see example :

```
label="FD_saint_gobain" # name used to identify your results
S2_year="2022" # last year of the S2 data, 2 years needed, e.g., 2021 & 2022
S2_path="/media/florian/data/data_S2" # path to S2 data, sorteb by tile
number_of_chunks='200'  # images are processed in small chunk to save RAM
list_tiles='T31UEQ' # eg, 'T31UDP T31TDN T30TYT'
v_model='v3' # v3 or v3_early_may available, see folder models
mask_final_maps='True'  # True (mask the final maps according to a binary mask, e.g., oak / not oaks) or False (maps are not masked)
path_to_binary_mask='' # If empty (''), we use OSO deciduous tree mask (2021)
scheduler_type='localCluster' # Choose Slurm (or other) if you work on a HPC, otherwise keep localCluster
nb_parallel_tasks='1'
```

#### Note: the model uses 2 years of data (from 01 Y1 Jan to 31 dec Y2). The model v3_early uses 1.5 years (Jan. To May)

#### Note: The produced map are available in results/your_production_label/final/. Other folders can be useful, particularly the log folder which contains potential production errors.

#### Note: you S2 data folder is supposed to look lile this /S2_data/Year/tile/ . For each year (2020, 2021, etc.) and each tile we store 2 years of data. You can use the routine /utils/create_symlinks_2y.py if you need help for this.