import rasterio
import glob
import numpy as np
from utils.utils_find_path import updateClusters, updateClusters_parallel
from rasterio.windows import Window
import os


if __name__ == "__main__":

    """
    This routine can be used to post process maps in order to remove oscillations between years. 
    It is inspired by https://arxiv.org/abs/1706.03161 and https://github.com/davidhallac/TICC
    
    The routine is experimental for now, you can modify it depending on your use case
    
    !! You need proba maps and classification maps !!
    """

    list_year = ['2017',
                 '2018',
                 '2019',
                 "2020",
                 '2021',
                 '2022',
                 '2023'
    ]

    root = '/work/RECONFORT/prod_pine_chestnut/pine/'

    v_dir = ""
    version_additional_str = ""

    list_raster_proba = []
    for year in list_year:
        out = root + v_dir + '/' + year + version_additional_str + '_probability_map'
        out = root + v_dir + '/' + year + '/final/' "ProbabilityMap_seed_0"
        list_raster_proba.append(rasterio.open(out + '.tif'))

    Wnd = Window.from_slices((17500, 20000), (17000, 21000))  # orleans

    width = list_raster_proba[0].width
    height = list_raster_proba[0].height
    proba_2017 = list_raster_proba[0].read(1)
    print(proba_2017.shape)

    N = 10
    rows_per_chunk = height // N
    cols_per_chunk = width // N
    remainder_rows = height % N
    remainder_cols = width % N

    for i in range(N):
        for j in range(N):
            print(i, j)
            # Compute the starting row and column indices for the current chunk
            row_start = i * rows_per_chunk
            col_start = j * cols_per_chunk
            # Compute the number of rows and columns for the current chunk
            rows = rows_per_chunk
            cols = cols_per_chunk
            if i == N - 1:
                rows += remainder_rows
            if j == N - 1:
                cols += remainder_cols

            # proba_2017 = list_raster_proba[0].read(1)

            Wnd = Window(col_start, row_start, cols, rows)
            proba_2017 = list_raster_proba[0].read(1, window=Wnd)
            # mask = rasterio.open(src_mask)
            # data_mask = mask.read(1, window=Wnd, boundless=True)

            n_samples = proba_2017.shape[0]*proba_2017.shape[1]
            n_classes = 3

            y_proba_time_all_classes = np.zeros([n_samples, len(list_year), n_classes])

            for year in range(len(list_year)):
                for class_n in range(n_classes):
                    proba_temp = list_raster_proba[year].read(class_n+1, window=Wnd)
                    # proba_temp = data_mask*proba_temp
                    y_proba_time_all_classes[:, year, class_n] = proba_temp.reshape(-1)

            sum_proba = np.sum(np.sum(y_proba_time_all_classes, axis=2), axis=1)
            masked_data = np.where(sum_proba == 0)
            # masked_data = np.where(mask == 0)

            y_pred_all_time = np.argmax(y_proba_time_all_classes, axis=2)

            paths = np.zeros([n_samples, len(list_year)])
            N_samples_not_masked = np.sum(sum_proba != 0)

            if N_samples_not_masked != 0:
                paths[sum_proba != 0, :] = updateClusters_parallel(LLE_node_vals=-y_proba_time_all_classes[sum_proba !=0, :, :], switch_penalty=0.15*1000)
                paths[sum_proba != 0, :] += np.ones(paths[sum_proba != 0, :].shape)

            i_year = 0
            for year in list_year:
                # out = '/home/florian/Images/mapping/' + v_dir + '/' + year + version_additional_str + '_probability_map'
                out = root + v_dir + '/' + year


                path_year = paths[:, i_year].reshape(proba_2017.shape[0], proba_2017.shape[1])
                print(path_year.shape)
                # with rasterio.Env():
                    # Write an array as a raster band to a new 8-bit file. For
                    # the new file's profile, we start with the profile of the source
                profile = list_raster_proba[0].profile

                    # And then change the band count to 1, set the
                    # dtype to uint8, and specify LZW compression.
                profile.update(
                    dtype=rasterio.uint8,
                    count=1,
                    compress='lzw')

                if os.path.isfile(out + '_corrected_viterbi.tif'):
                    print("file exist")
                    with rasterio.open(out + '_corrected_viterbi.tif', 'r+', **profile) as dst:
                        dst.write(path_year.astype(rasterio.uint8), 1, window=Wnd)
                else:
                    with rasterio.open(out + '_corrected_viterbi.tif', 'w', **profile) as dst:
                        dst.write(path_year.astype(rasterio.uint8), 1, window=Wnd)
                i_year += 1

    # compress_final maps
    list_maps = glob.glob(root + v_dir + '/*viterbi.tif' )
    print(list_maps)

    for img in list_maps:

        src = rasterio.open(img)
        profile = src.profile

        profile = list_raster_proba[0].profile
        profile.update(
            dtype=rasterio.uint8,
            count=1,
            compress='lzw')

        new_name = img[0: len(img)-4] + '_lzw.tif'
        print(new_name)
        with rasterio.open(new_name, 'w', **profile) as dst:
            dst.write(src.read(1).astype(rasterio.uint8), 1)
