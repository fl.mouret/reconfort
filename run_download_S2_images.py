import subprocess
from argparse import RawTextHelpFormatter
import argparse
from utils.utils import load_config_variable

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="This routine download S2 images using the theia_download package "
                    "(see https://github.com/olivierhagolle/theia_download )",
        formatter_class=RawTextHelpFormatter
    )
    parser.add_argument(
        "-config_file",
        dest="config_file",
        help=
        "Config file with mandatory variables for theia_download\n"
        "You can create an account here : You can create a theia account https://www.theia-land.fr \n"
        "EXAMPLE OF CFG FILE: \n \n"
        "tile='T31TCJ' # tile to be downloaded \n"
        "path_to_cfg_theia_account='/media/florian/config_theia.cfg' # theia account information \n"
        "start='2018-10-15'  # Start date \n"
        "end='2018-12-15'  # End date' \n"
        "zip_path='/data/S2_data/zip/year/' # path to download the data \n"
        "out_dir='/data/S2_data/extracted/year/' # path to download the data \n"
        ,
        required=True
    )
    args = parser.parse_args()
    config_file = args.config_file
    command_path = 'utils/theia_download.py'

    dict_config = load_config_variable(config_file)
    print('list of user variables')
    print(dict_config)

    subprocess.run([
        "python", command_path,
        '-t', dict_config['tile'],
        '-c', 'SENTINEL2',
        '-a', dict_config['path_to_cfg_theia_account'],
        '-d', dict_config['start'],
        '-f', dict_config['end'],
        '-m', '95',
        '-w', dict_config['zip_path']
    ])
    # "run theia_download.py -t T31TCJ -c SENTINEL2 -a config_theia.cfg -d 2018-10-15 -f 2018-12-15 -m 20 -w /data/S2_data/raw_data/"