import os
import glob

if __name__ == '__main__':

    source_dir = "/work/RECONFORT/iota2_project_reconfort/s2_data/"
    out_dir = "/work/RECONFORT/iota2_project_reconfort/s2_data_annual_2y/"

    list_tiles = glob.glob(source_dir + '/*/')

    list_years = ['2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023']
    # list_years = ['2022']
    # list_years = ['2021']
    # list_years = ['2023']

    start_nov = True

    for year in list_years:
        if not os.path.isdir(out_dir + year):
            os.makedirs(out_dir + year)

        for tile_dir in list_tiles:
            tile = os.path.basename(os.path.normpath(tile_dir))

            if not os.path.isdir(out_dir + year + '/' + tile):
                os.makedirs(out_dir + year + '/' + tile)
            print(tile)
            list_merged_img = glob.glob(tile_dir + '/*_' + year + '*/')

            print(list_merged_img)

            for merged_img in list_merged_img:
                name_img = os.path.basename(os.path.normpath(merged_img))
                # create simlink merged images
                os.makedirs(out_dir + year  + '/' + tile + '/' + name_img)
                os.makedirs(out_dir + year + '/' + tile + '/' + name_img + '/MASKS')

                list_tif = list_tif = glob.glob(merged_img + '/*.tif')
                for tif in list_tif:
                    os.symlink(tif, out_dir + year + '/' + tile + '/' + name_img + '/'
                               + os.path.basename(os.path.normpath(tif)))

                list_masks = glob.glob(merged_img + '/*/*.tif')
                for mask in list_masks:
                    os.symlink(mask, out_dir + year + '/' + tile + '/' + name_img + '/MASKS/'
                               + os.path.basename(os.path.normpath(mask)))

            # N-1
            list_merged_img = glob.glob(tile_dir + '/*_' + str(int(year)-1) + '*/')
            print(list_merged_img)

            for merged_img in list_merged_img:
                name_img = os.path.basename(os.path.normpath(merged_img))
                # create simlink merged images
                os.makedirs(out_dir + year + '/' + tile + '/' + name_img)
                os.makedirs(out_dir + year + '/' + tile + '/' + name_img + '/MASKS')

                list_tif = list_tif = glob.glob(merged_img + '/*.tif')
                for tif in list_tif:
                    os.symlink(tif, out_dir + year + '/' + tile + '/' + name_img + '/'
                               + os.path.basename(os.path.normpath(tif)))

                list_masks = glob.glob(merged_img + '/*/*.tif')
                for mask in list_masks:
                    os.symlink(mask, out_dir + year + '/' + tile + '/' + name_img + '/MASKS/'
                               + os.path.basename(os.path.normpath(mask)))

            # for interpolation purpose, adding images coming from DEC YEAR-1
            list_merged_img = glob.glob(tile_dir + '/*_' + str(int(year)-2) + '12' + '*/')
            print(list_merged_img)
            for merged_img in list_merged_img:
                name_img = os.path.basename(os.path.normpath(merged_img))
                # create simlink merged images
                os.makedirs(out_dir + year + '/' + tile + '/' + name_img)
                os.makedirs(out_dir + year + '/' + tile + '/' + name_img + '/MASKS')

                list_tif = list_tif = glob.glob(merged_img + '/*.tif')
                for tif in list_tif:
                    os.symlink(tif, out_dir + year + '/' + tile + '/' + name_img + '/'
                               + os.path.basename(os.path.normpath(tif)))

                list_masks = glob.glob(merged_img + '/*/*.tif')
                for mask in list_masks:
                    os.symlink(mask, out_dir + year + '/' + tile + '/' + name_img + '/MASKS/'
                               + os.path.basename(os.path.normpath(mask)))

            # if start in nov, add Oct, Nov to the data folder
            if start_nov:
                for month in ['10', '11']:
                    list_merged_img = glob.glob(tile_dir + '/*_' + str(int(year) - 2) + month + '*/')
                    print(list_merged_img)
                    for merged_img in list_merged_img:
                        name_img = os.path.basename(os.path.normpath(merged_img))
                        # create simlink merged images
                        os.makedirs(out_dir + year + '/' + tile + '/' + name_img)
                        os.makedirs(out_dir + year + '/' + tile + '/' + name_img + '/MASKS')

                        list_tif = list_tif = glob.glob(merged_img + '/*.tif')
                        for tif in list_tif:
                            os.symlink(tif, out_dir + year + '/' + tile + '/' + name_img + '/'
                                       + os.path.basename(os.path.normpath(tif)))

                        list_masks = glob.glob(merged_img + '/*/*.tif')
                        for mask in list_masks:
                            os.symlink(mask, out_dir + year + '/' + tile + '/' + name_img + '/MASKS/'
                                       + os.path.basename(os.path.normpath(mask)))