import geopandas as gpd
from shapely.geometry import Point
import random
import os
import geopandas as gpd
import numpy as np

def generate_random_point_in_bbox(bbox):

    polygon_radius = 0.001
    min_x, min_y, max_x, max_y = bbox
    random_x = random.uniform(min_x, max_x)
    random_y = random.uniform(min_y, max_y)
    random_point = Point(random_x, random_y)
    polygon = random_point.buffer(polygon_radius)
    return polygon



if __name__ == "__main__":

    """
    This script generate 3 random point for each tile. 
    This is used in iota2, which need to have at least one point of each class in the area 
    of the produced the classification map
    """

    new_root = os.path.dirname(os.path.abspath(__file__))
    os.chdir(new_root)

    # list_tiles = ['T31UDQ',
    #               'T31UDP',
    #               'T31UCQ',
    #               'T31UCP',
    #               'T31TEN',
    #               'T31TEM',
    #               'T31TDN',
    #               'T31TDM',
    #               'T31TCN',
    #               'T31TCM',
    #               'T30TYT'
    #               ]

    # we use a shapefile where each row is the envelope of a specific S2 tile in France
    shp = gpd.read_file('/media/florian/data/data_S2/tiles/Tiles_S2_France_l93_withCorse.shp')

    point_names = []
    point_classes = []
    point_geometries = []

    for tile in np.unique(shp['Name']):
        # envelope_shapefile_path = new_root + '/EmpriseTuiles/' + tile + '.shp'
        # envelope_gdf = gpd.read_file(envelope_shapefile_path)
        envelope_gdf = shp.iloc[np.where(shp['Name'] == tile)[0]]
        envelope_gdf = envelope_gdf.to_crs('EPSG:2154')

        min_x, min_y, max_x, max_y = envelope_gdf.total_bounds
        tile_bbox = (min_x, min_y, max_x, max_y)

        for class_dep in [1, 2, 3]:
            point_geometries.append(generate_random_point_in_bbox(tile_bbox))
            point_classes.append(class_dep)
            point_names.append(tile + str(class_dep))

    data = {'geometry': point_geometries, 'dep_cor': point_classes, 'point_names': point_names}
    print(envelope_gdf.crs)
    gdf = gpd.GeoDataFrame(data, crs=envelope_gdf.crs)
    gdf = gdf.to_crs('EPSG:2154')

    # Specify the output shapefile path and save the GeoDataFrame to the shapefile
    output_shapefile = 'random_points.shp'
    gdf.to_file(output_shapefile)
