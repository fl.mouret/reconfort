import numpy as np
from joblib import Parallel, delayed

def updateClusters(LLE_node_vals, switch_penalty=1):
    """
    Takes in LLE_node_vals matrix and computes the path that minimizes
    the total cost over the path
    Note the LLE's are negative of the true LLE's actually!!!!!
    Note: switch penalty > 0
    """

    path = np.zeros([LLE_node_vals.shape[0], LLE_node_vals.shape[1]])
    T = LLE_node_vals.shape[1]
    num_clusters = LLE_node_vals.shape[2]

    print(path.shape)
    print(T)
    print(num_clusters)

    for sample in range(LLE_node_vals.shape[0]):
        # print(sample)
        # (T, num_clusters) = LLE_node_vals.shape
        future_cost_vals = np.zeros([LLE_node_vals.shape[1], LLE_node_vals.shape[2]])
        # compute future costs
        for i in range(T-2, -1, -1):
            j = i+1
            indicator = np.zeros(num_clusters)
            future_costs = future_cost_vals[j, :]
            lle_vals = LLE_node_vals[sample, j, :]
            for cluster in range(num_clusters):
                total_vals = future_costs + lle_vals + switch_penalty
                total_vals[cluster] -= switch_penalty
                future_cost_vals[i, cluster] = np.min(total_vals)

        # compute the best path
        # the first location
        curr_location = np.argmin(future_cost_vals[0, :] + LLE_node_vals[sample,0, :])
        path[sample, 0] = curr_location

        # compute the path
        for i in range(T-1):
            j = i+1
            future_costs = future_cost_vals[j, :]
            lle_vals = LLE_node_vals[sample, j, :]
            total_vals = future_costs + lle_vals + switch_penalty
            total_vals[int(path[sample, i])] -= switch_penalty

            path[sample, i+1] = np.argmin(total_vals)

        # return the computed path
    return path


def routine_path_one_sample(LLE_node_vals, switch_penalty, T, num_clusters):
    path_sample = np.zeros(LLE_node_vals.shape[0])
    future_cost_vals = np.zeros([LLE_node_vals.shape[0], LLE_node_vals.shape[1]])
    # compute future costs
    for i in range(T - 2, -1, -1):
        j = i + 1
        # indicator = np.zeros(num_clusters)
        future_costs = future_cost_vals[j, :]
        lle_vals = LLE_node_vals[j, :]
        for cluster in range(num_clusters):
            total_vals = future_costs + lle_vals + switch_penalty
            total_vals[cluster] -= switch_penalty
            future_cost_vals[i, cluster] = np.min(total_vals)
    # compute the best path
    # the first location
    curr_location = np.argmin(future_cost_vals[0, :] + LLE_node_vals[0, :])
    path_sample[0] = curr_location

    # compute the path
    for i in range(T - 1):
        j = i + 1
        future_costs = future_cost_vals[j, :]
        lle_vals = LLE_node_vals[j, :]
        total_vals = future_costs + lle_vals + switch_penalty
        total_vals[int(path_sample[i])] -= switch_penalty
        path_sample[i + 1] = np.argmin(total_vals)
    # return the computed path
    return path_sample


def updateClusters_parallel(LLE_node_vals, switch_penalty=1):
    """
    Takes in LLE_node_vals matrix and computes the path that minimizes
    the total cost over the path
    Note the LLE's are negative of the true LLE's actually!!!!!
    Note: switch penalty > 0
    """
    path = np.zeros([LLE_node_vals.shape[0], LLE_node_vals.shape[1]])
    T = LLE_node_vals.shape[1]
    num_clusters = LLE_node_vals.shape[2]
    path = Parallel(n_jobs=16)(delayed(routine_path_one_sample)(LLE_node_vals[i,:,:], switch_penalty, T, num_clusters) for i in range(LLE_node_vals.shape[0]))

    return path