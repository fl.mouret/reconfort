# Train a multi-year classification

####  Note: This sub-package is still under development (it was originally designed for one use case)

Based on the work described in https://doi.org/10.1109/JSTARS.2023.3332420

This work was supported by the University of Orleans, CESBIO laboratory, University of Toulouse III Paul Sabatier within the
SYCOMORE program, with the financial support of the Région Centre-Val de Loire (France).

#### Objective: 
- We aim at train a model using reference data acquired at different dates/years. 
- **Why we don't use directly iota2 to train a model based on some reference data ?** If we have labeled data (potentially revisited) at different dates, we can't use iota2 directly because each reference sample has to be associated with a time series acquired at different time instants (see Fig.7 in [the associated paper](https://doi.org/10.1109/JSTARS.2023.3332420)).     

### Cite:

F. Mouret, D. Morin, H. Martin, M. Planells and C. Vincent-Barbaroux, "Toward an Operational Monitoring of Oak Dieback With Multispectral Satellite Time Series: A Case Study in Centre-Val De Loire Region of France," in IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing, doi: 10.1109/JSTARS.2023.3332420.
```
@ARTICLE{mouret_2023,
  author={Mouret, Florian and Morin, David and Martin, Hilaire and Planells, Milena and Vincent-Barbaroux, Cécile},
  journal={IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing}, 
  title={Toward an Operational Monitoring of Oak Dieback With Multispectral Satellite Time Series: A Case Study in Centre-Val De Loire Region of France}, 
  year={2023},
  volume={},
  number={},
  pages={1-18},
  doi={10.1109/JSTARS.2023.3332420}}
```

## Getting started : 

#### We suppose that you already have installed you conda package (see the root of this project for detailed instructions).

## Run main training scripts

After having installed your conda env, don't forget to activate it before running the reconfort package:

``` conda activate my_new_i2_env ```

### 1) Download and unzip images 

See [root of the project for detailled instruction](https://framagit.org/fl.mouret/reconfort#1-run_download_s2_images). 

### 2) run_extract_learning_samples_by_years_and_label_years

- This routine will extract a sqlite with S2 times series for **each specified label year.**

```
python run_extract_learning_samples_by_years_and_label_years.py -config_file your_config_file.cfg  
```

#### A Config file is needed, see example below (or in the folder examples_config_files):

```
label="oak" # name used to identify your results
name_task="extract_oak"# name folder to extract files 
S2_path="/work/RECONFORT/iota2_project_reconfort/s2_data_annual" # path to S2 data, sorteb by tile
number_of_chunks='100'  # images are processed in small chunk to save RAM
list_tiles='T31UDQ T31UDP T31UCQ T31UCP T31TEN T31TEM T31TDN T31TDM T31TCN T31TCM T30TYT' # eg, 'T31UDP T31TDN T30TYT'
scheduler_type='Slurm'
nb_parallel_tasks='10'
ground_truth_path='/work/RECONFORT/iota2_project_reconfort/vector_db/chene_fusion_buf_WGS_2154_LAMB93_without_roads_with_month_' # will be changed using the label year
list_S2_years='2022 2023' # careful, we split using white spaces, i.e., '2022 2023' will become ['2022', '2023']
list_label_years='2017 2018 2019 2020 2021 2022' # careful, we split using white spaces, i.e., '2022 2023' will become ['2022', '2023']
out_dir_root_work_iota2='/work/scratch/data/mouretf/'
out_dir_root_save_feat='/work/scratch/data/mouretf/extracted_samples/'
```

**IMPORTANT: Shapefile name formatting**

The format is as follows: ground_truth_path + label_year + '.shp' 

**Exemple with two labeling years:** 
- /home/myproject/ground_truth/my_oak_labels_2020.shp
- /home/myproject/ground_truth/my_oak_labels_2022.shp

In that case: 
- ground_truth_path= /home/myproject/ground_truth/my_oak_labels_
- list_label_years='2020 2022'

**IMPORTANT: Mandatory columns needed in your shapefiles**

- 'dep_cor' : your label column, e.g., 1 = healthy, 2 = dieback, 3 = heavy dieback. **DO NOT USE 0 IN YOUR LABELS (confusion with missing data)**
- DEPERIS_pe : a float in the range [0, 1] related to the percentage of declining tree, i.e., %D+. Used to separate in 2 or 3 classes.
- 'id_unique' : ID that identifies each plot 
- 'date' : year of labeling, e.g., 2020

### 3) merge sqlite files and reinterpolate dataframe

Once we have extracted sqlite files for each year and label year, we need to:
- 3.1) merge the extracted files into a unique CSV file

```
python utils_merge_sqlites_from_iota2.py --dir_shapefile /vector_db/ --end_shp_file chene_fusion_ --gf_data_root extracted_samples/oak/ --output_folder merged_samples_oak_v4  
```

- 3.2) re-interpolate this CSV file to have time series that will be equivalent to those used in iota2 (i.e., 1 image every 10D between 1st Jan. of Y1 and 29 oct of Y2)

```
python utils_reinterpolate_dataframe.py --output_folder extracted_samples/oak/ 
```

### 4) run_train_and_save_model_for_iota2_v3

- This routine train a iota2 RF model based on training samples acquired over time and following the method detailed in [the associated paper](https://doi.org/10.1109/JSTARS.2023.3332420).
- Some improvement should be made for better deployment (some parameters are still hardcoded etc.) You can modify the script, but be careful !
- Before training the final model, a CV is performed and can be used to analyze the performances of your model.

```
python run_train_and_save_model_for_iota2_v3.py -config_file your_config_file.cfg  
```

#### A Config file is needed, see example below (or in the folder examples_config_files):

```
model_name = 'v_oak_test'  # name used to identify your results
n_classes='3' # number of classes, 2 (healthy, dieback) or 3 (healthy, dieback, heavy dieback (%D+ >50%)) 
path_to_csv='/extracted_samples_oak/merged_samples/
```