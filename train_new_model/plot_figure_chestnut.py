import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

def compute_mean_std_list_results(dict_results_metric, n_exp):
    list_mean = []

    for i_exp in range(len(dict_results_metric[2017])):
        list_BA = []
        for year in dict_results_metric.keys():
            list_BA.append(dict_results_metric[year][i_exp])

        list_mean.append(np.nanmean(list_BA))
    # print(list_mean)
    print(np.nanmean(list_mean))
    print(np.nanstd(list_mean))
    print(np.nanstd(list_mean)*1.96/np.sqrt(exp))
    return list_mean


def plot_results_paper(dict_results,
                       list_keys, list_colors, list_ls, list_labels,
                       exp,
                       add_mean_all_years=False,
                       add_legend=True,
                       add_legend2=True,
                       y_title=False
                       ):
    cycle_colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
                    '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
                    '#bcbd22', '#17becf', 'black', 'gray']

    fig, ax = plt.subplots(dpi=250)
    step = 0

    for key, color, ls, label in zip(list_keys, list_colors, list_ls, list_labels):
        list_mean = []
        list_CI = []
        list_uplims = []

        list_years_keys = list(dict_results[key].keys())

        if add_mean_all_years:
            list_allyears = []
            for year in dict_results[key].keys():
                list_allyears.append(dict_results[key][year])
            dict_results[key]['Mean all years'] = list_allyears
            list_years_keys.insert(0, 'Mean all years')

        for year in list_years_keys:
            if add_mean_all_years and year == 'Mean all years':
                list_mean.insert(0, np.nanmean(dict_results[key][year]))
                list_CI.insert(0, 1.96 * np.nanstd(dict_results[key][year]) / np.sqrt(exp*6))
                list_uplims.append(True)
            else:
                list_mean.append(np.nanmean(dict_results[key][year]))
                list_CI.append(1.96 * np.nanstd(dict_results[key][year]) / np.sqrt(exp))
                list_uplims.append(True)

        name = ax.errorbar(np.arange(len(list_years_keys)) + step, list_mean,
                           yerr=list_CI,
                           # uplims=list_uplims, lolims=False,
                           capsize=4,
                           marker='.',
                           # yerr= [i * 1.96/np.sqrt(30*3) for i in list_std_OA_augmentation],
                           color=cycle_colors[color], label=label,
                           linestyle='')
        name[-1][0].set_linestyle(ls)
        step += 0.1

    for cat in range(len(list_years_keys)):
        # list_lines.append(cat - 0.2)
        # list_lines.append(cat + 0.2)
        ax.axvline(cat - 0.2, color='gray', linewidth=0.9)  # vertical lines
        ax.axvline(cat + step + 0.2, color='gray', linewidth=0.9)  # vertical lines

    plt.xticks(np.arange(len(list_years_keys)) + step/2, list_years_keys)
    ax.grid(linestyle=':')
    ax.xaxis.grid(False)
    custom_lines = [Line2D([0], [0], color='black', linestyle='-'),
                    Line2D([0], [0], color='black', linestyle=':'),
                    ]
    if add_legend2:
        leg = ax.legend(custom_lines, ['2 cl', '3 cl'],
                        loc='upper right',
                        bbox_to_anchor=(0.5, 0.22),
                        )
        ax.add_artist(leg)

    if add_legend or add_legend2:
        ax.legend()

    if y_title:
        ax.set_ylabel(y_title)
    plt.show()


if __name__ == "__main__":

    # this routine can be used to plot classification results, as in 10.1109/JSTARS.2023.3332420
    # Experimental routine

    selected_band = '_B_'
    selected_band = 'CR_'
    # selected_band = 'CRB8AB11B12_'
    # selected_band = 'CRswirB8AB11B12_'
    # selected_band = 'B8AB11B12_'

    # selected_band = 'CRswir_'
    selected_dates = 'all_dates_'
    N_year_used = 2
    exp = 50
    ext = ''
    # ext = '_Nm3Np2'
    ext = 'stratified_error_v2'

    cycle_colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
                    '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
                    '#bcbd22', '#17becf', 'black', 'gray']


    extension = selected_band + '_' + selected_dates + '_Ny' + str(N_year_used) + 'Nexpe_' + str(exp) + ext

    path_results = 'results_experiments/CV_by_year/results_' + extension + '_change_Dsure_yeartest.npy'
    # path_results = 'results_experiments/CV_by_year/results_' + extension + '.npy'
    path_results = 'results_experiments/CV_DA/results_' + extension + '.npy'

    path_results = 'result_classif/results_CR__all_dates__end_early_11_Ny2Nexpe_5_chestnut.npy'
    # path_results = 'result_classif/results_CR__all_dates__end_early_11_Ny2Nexpe_5_chestnut_cond1_09.npy'

    dict_results = np.load(path_results, allow_pickle=True).item()

    print(dict_results.keys())
    list_types = ['raw', 'DA']
    # dict_results['BA_2cl_DA'] = dict_results['dict_BA_augmentation_2cl']

    list_keys = ['BA_raw', 'BA_2cl_raw', 'BA_DA', 'BA_2cl_DA']
    # list_keys = ['OA_raw', 'OA_DA', 'BA_raw', 'BA_DA']
    list_ls = [':', '-', ':', '-']
    list_colors = [0, 0, 1, 1]
    # list_labels = ['OA (2 cl.)', '', 'BA (3 cl.)', '', 'F1 (3 cl.)', '']
    list_labels = ['', 'Raw reference data', '', 'Expanded reference data']
    y_title = 'Balanced Accuracy'
    plot_results_paper(dict_results, list_keys, list_colors, list_ls, list_labels, exp, add_mean_all_years=True, add_legend2=True, y_title=y_title)

    list_keys = ['OA_raw', 'OA_2cl_raw', 'OA_DA', 'OA_2cl_DA']
    # list_keys = ['OA_raw', 'OA_DA', 'BA_raw', 'BA_DA']
    list_ls = [':', '-', ':', '-']
    list_colors = [0, 0, 1, 1]
    # list_labels = ['OA (2 cl.)', '', 'BA (3 cl.)', '', 'F1 (3 cl.)', '']
    list_labels = ['', 'Raw reference data', '', 'Expanded reference data']
    y_title = 'Overall Accuracy'
    plot_results_paper(dict_results, list_keys, list_colors, list_ls, list_labels, exp, add_mean_all_years=True,
                       add_legend2=True, y_title=y_title)

    list_keys = ['recall0_raw', 'precision0_raw', 'recall0_DA', 'precision0_DA']
    # list_keys = ['OA_raw', 'OA_DA', 'BA_raw', 'BA_DA']
    list_ls = ['-', ':', '-', ':']
    list_colors = [0, 0, 1, 1]
    # list_labels = ['OA (2 cl.)', '', 'BA (3 cl.)', '', 'F1 (3 cl.)', '']
    list_labels = ['Raw reference data', '', 'Augmented reference data', '']
    y_title = 'Recall (-) and precision (:) (healthy)'
    plot_results_paper(dict_results, list_keys, list_colors, list_ls, list_labels, exp, add_mean_all_years=True, add_legend2=False,
                       y_title=y_title)

    list_keys = ['F1_0_raw', 'F1_1bin_raw', 'F1_0_DA', 'F1_1bin_DA']
    # list_keys = ['OA_raw', 'OA_DA', 'BA_raw', 'BA_DA']
    list_ls = ['-', ':', '-', ':']
    list_colors = [0, 0, 1, 1]
    # list_labels = ['OA (2 cl.)', '', 'BA (3 cl.)', '', 'F1 (3 cl.)', '']
    list_labels = ['Raw reference data', '', 'Augmented reference data', '']
    y_title = 'F1 healthy (-) and F1 dieback (:)'
    plot_results_paper(dict_results, list_keys, list_colors, list_ls, list_labels, exp, add_mean_all_years=True, add_legend2=False,
                       y_title=y_title)

    list_keys = ['BA_2cl_raw', 'BA_2cl_DA']
    # list_keys = ['OA_raw', 'OA_DA', 'BA_raw', 'BA_DA']
    list_ls = ['-', '-']
    list_colors = [0, 1]
    # list_labels = ['OA (2 cl.)', '', 'BA (3 cl.)', '', 'F1 (3 cl.)', '']
    list_labels = ['Raw reference data', 'Augmented reference data']
    y_title = 'Balanced Accuracy (2 cl.)'
    plot_results_paper(dict_results, list_keys, list_colors, list_ls, list_labels, exp, add_mean_all_years=True, add_legend2=True,
                       y_title=y_title)

    list_keys = ['OA_2cl_raw', 'OA_2cl_DA', 'BA_2cl_raw', 'BA_2cl_DA', 'mean_F1_bin_raw', 'mean_F1_bin_DA']
    # list_keys = ['OA_raw', 'OA_DA', 'BA_raw', 'BA_DA']
    list_ls = [':', '-', ':', '-', ':', '-']
    list_colors = [0, 0, 2, 2, 3, 3]
    list_labels = ['OA (2 cl.)', '', 'BA (2 cl.)', '', 'F1 (2 cl.)', '']
    plot_results_paper(dict_results, list_keys, list_colors, list_ls, list_labels, exp, add_mean_all_years=True)

    list_keys = ['OA_raw', 'OA_DA', 'BA_raw', 'BA_DA', 'mean_F1_raw', 'mean_F1_DA']
    # list_keys = ['OA_raw', 'OA_DA', 'BA_raw', 'BA_DA']
    list_ls = [':', '-', ':', '-', ':', '-']
    list_colors = [0, 0, 2, 2, 3, 3]
    list_labels = ['OA (3 cl.)', '', 'BA (3 cl.)', '', 'F1 (3 cl.)', '']
    plot_results_paper(dict_results, list_keys, list_colors, list_ls, list_labels, exp, add_mean_all_years=True)

    list_keys = ['OA_2cl_raw', 'OA_2cl_DA', 'BA_2cl_raw', 'BA_2cl_DA', 'mean_F1_bin_raw', 'mean_F1_bin_DA']
    # list_keys = ['OA_raw', 'OA_DA', 'BA_raw', 'BA_DA']
    list_ls = [':', '-', ':', '-', ':', '-']
    list_colors = [0, 0, 2, 2, 3, 3]
    list_labels = ['OA (2 cl.)', '', 'BA (2 cl.)', '', 'F1 (2 cl.)', '']
    plot_results_paper(dict_results, list_keys, list_colors, list_ls, list_labels, exp, add_mean_all_years=True)