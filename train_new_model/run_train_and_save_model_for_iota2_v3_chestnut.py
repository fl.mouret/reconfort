from sklearn.experimental import enable_iterative_imputer  # noqa
from sklearn.impute import IterativeImputer
from sklearn.ensemble import ExtraTreesRegressor
from train_new_model.utils.utils_ts_analysis import *
from train_new_model.utils.utils_compute_indices import *
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from scipy.stats import mode
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import SMOTE
from train_new_model.utils.py_shark_rf_classifier import train_shark_rf, predict_shark_rf


def adjacent_values(vals, q1, q3):
    upper_adjacent_value = q3 + (q3 - q1) * 1.5
    upper_adjacent_value = np.clip(upper_adjacent_value, q3, vals[-1])

    lower_adjacent_value = q1 - (q3 - q1) * 1.5
    lower_adjacent_value = np.clip(lower_adjacent_value, vals[0], q1)
    return lower_adjacent_value, upper_adjacent_value


def reorder_df_2D_learning(df_init, select_order_feat=False):
    '''
    We want to transform df_init of size N_samples x N_features in df_ordered of size n_samples x N_sensors x T, with T the length of the time series
    :param df: init dataset
    :return:
    '''

    n_samples = df_init.shape[0]
    list_col = list(df_init.columns.values)
    print(list_col)
    list_dates = list(set([s[0:8] for s in list_col]))
    list_dates.sort()
    print(list_dates)
    T = len(list_dates)

    list_feat = list(set([s[9::] for s in list_col]))
    print(list_feat)
    if select_order_feat is not False:
        # list_feat = ['B2', 'B3', 'B4', 'B8A', 'B11', 'CRswir', 'B6', 'B8', 'CRre', 'B7', 'B5', 'B12']
        list_feat = select_order_feat
        print("ordered list feat", list_feat)
    n_sensors = len(list_feat)
    array_ordered = np.empty([n_samples, n_sensors, T])

    i_date = 0
    for date in list_dates:
        i_feat = 0
        for feat in list_feat:
            array_ordered[:, i_feat, i_date] = df_init[date + '_' + feat]
            i_feat += 1
        i_date += 1

    print("T=" + str(T))
    print("N_sensors=" + str(n_sensors))

    return array_ordered, T, n_samples, list_dates, list_feat


def sort_df_for_classif_old(data, n_year, acq_date):

    first_sample = True
    for sample in range(data.shape[0]):
        acq_date_sample = acq_date[sample]
        list_samples_year = np.arange(int(acq_date_sample) - n_year + 1, int(acq_date_sample) + 1)
        list_samples_dates = []
        for year in list_samples_year:
            for col in data.columns:
                if col[0:4] in str(year):
                    list_samples_dates.append(col)
        list_samples_dates.sort()
        # print(list_samples_dates)

        if first_sample:
            first_sample = False
            data_sort = np.zeros([data.shape[0], len(list_samples_dates)])

        data_sort[sample, :] = data[list_samples_dates].values[sample, :]

    return data_sort


def sort_df_for_classif(data, n_year, acq_date, start_nov=False, end_early=False, month_early='', return_col=False):

    first_year = True

    if np.logical_and(start_nov, end_early):
        raise ValueError("start nov and end early not coded")

    if start_nov:
        # using data starting from Nov. to Oct.
        print(np.unique(acq_date))
        for sel_year in np.unique(acq_date):

            idx_year = np.where(acq_date == sel_year)[0]
            list_samples_year = np.arange(int(sel_year) - n_year + 1, int(sel_year) + 1)
            list_samples_dates = []
            # print(list_samples_year)
            for year in list_samples_year:
                for col in data.columns:

                    year_col = col[0:4]
                    if year_col == 'date':
                        pass
                    month_col = col[4:6]
                    # print(year, year_col, month_col)

                    cond1 = np.logical_and(int(year) -1 == int(year_col), int(month_col) > 10)  # nov or dec N-1
                    # print(cond1)
                    cond2 = np.logical_and(int(year) == int(year_col), int(month_col) < 11)  # from jan to oct N
                    # print(cond2)

                    if np.logical_or(cond1, cond2):
                        list_samples_dates.append(col)
            list_samples_dates.sort()
            print("starting from NOV")
            print(list_samples_dates)
            # err
            if first_year:
                first_year = False
                data_sort = np.zeros([data.shape[0], len(list_samples_dates)])
                list_col = list_samples_dates.copy()

            data_sort[idx_year, :] = data[list_samples_dates].values[idx_year, :]

    elif end_early:
        # using data starting from Jan. to Oct.
        print(np.unique(acq_date))
        for sel_year in np.unique(acq_date):

            idx_year = np.where(acq_date == sel_year)[0]
            list_samples_year = np.arange(int(sel_year) - n_year + 1, int(sel_year) + 1)
            list_samples_dates = []
            # print(list_samples_year)
            for year in list_samples_year:
                for col in data.columns:

                    year_col = col[0:4]
                    if year_col == 'date':
                        continue
                    month_col = col[4:6]
                    day_col = col[6:8]
                    # print(month_col, day_col)
                    # print(year, year_col, month_col)

                    # cond1 = np.logical_and(int(year) - 1 == int(year_col), int(month_col) > 10)  # nov or dec N-1
                    # print(cond1)
                    if year == list_samples_year[-1]:
                        if len(list_samples_year) > 1:

                            # Using two years --> interpolate data is problematic needs to remove 0101
                            if n_year > 2:
                                raise ValueError('Check the interpolation')
                            cond_0101 = np.logical_and(int(year) == int(year_col),
                                                       not np.logical_and(int(month_col) == 1,
                                                                          int(day_col) == 1))
                            cond_11 = np.logical_and(int(year) == int(year_col), int(month_col) <= int(month_early))
                            cond2 = np.logical_and(cond_11, cond_0101)# from jan to oct N
                        else:
                            cond2 = np.logical_and(int(year) == int(year_col), int(month_col) <= int(month_early))  # from jan to oct N
                    else:
                        cond2 = int(year) == int(year_col)
                    # print(cond2)

                    if cond2:
                        list_samples_dates.append(col)
            list_samples_dates.sort()
            print("end OCT")
            print(list_samples_dates)
            # err
            if first_year:
                first_year = False
                data_sort = np.zeros([data.shape[0], len(list_samples_dates)])
                list_col = list_samples_dates.copy()

            data_sort[idx_year, :] = data[list_samples_dates].values[idx_year, :]
    else:
        # using data starting from January of each year
        for sel_year in np.unique(acq_date):

            idx_year = np.where(acq_date == sel_year)[0]
            list_samples_year = np.arange(int(sel_year) - n_year + 1, int(sel_year) + 1)
            list_samples_dates = []
            for year in list_samples_year:
                for col in data.columns:
                    if col[0:4] in str(year):
                        list_samples_dates.append(col)
            list_samples_dates.sort()
            print(list_samples_dates)
            if first_year:
                first_year = False
                data_sort = np.zeros([data.shape[0], len(list_samples_dates)])
                list_col = list_samples_dates.copy()

            data_sort[idx_year, :] = data[list_samples_dates].values[idx_year, :]

    if return_col:
        return data_sort, list_col
    else:
        return data_sort


if __name__ == "__main__":

    # print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
    # print(torch.cuda.is_available())
    # # err

    cycle_colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
                    '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
                    '#bcbd22', '#17becf', 'black', 'gray']


    # ////////////
    # List of key parameters to be used

    # freq of interpolation. 10D_0101_1231 means each 10 D between 0101 and 12 31. See reinterpolate_dataframe function
    freq = ""
    freq = '10D'
    freq = '10D_0101_1231'

    # N year used to train the model
    N_year_used = 2

    # Number of classes
    n_classes = 3

    # start data in november (True / False). End in october instead of December (True /false)
    start_nov = False
    end_early = True
    month_early = '10'

    # use SMOTE
    use_SMOTE = True
    # Balance dataset with SMOTE each year
    balance_each_year = True
    # Use DF SURE for data augmentation (see cond1 & cond 2)
    data_augmentation = True

    # Selected band (in list). In brief: each feature with name contained in this list is selected
    # e.g.:: B_ --> all bands since B_2 is in B_ etc. CR: CRswir and CRre since CR is in CRswir
    selected_band = [  # '_B',
        'CR',
        # 'CRswir',
    ]

    # For tests only: use raw data, impute mising data, add cloud mask...
    from_raw_data = ''
    impute_algo = ''
    # filtering clouds
    cloud_mask = False
    cloud_mask_str = ''
    impute_feat = False
    select_labels = False

    # Adding some string to the model name
    if start_nov:
        cloud_mask_str += '_start_in_NOV'
        str_nov = '_start_in_NOV'
    else:
        if end_early:
            str_nov = 'end_ind_' + str(month_early)
        else:
            str_nov = ''
    # cloud_mask_str += '_only_2020_2021_labels'
    if use_SMOTE:
        str_nov += 'SMOTE'

    if balance_each_year:
        str_nov += 'balance_each_year'

    if data_augmentation:
        str_nov += '_data_augmentation'

    v_merged_samples = "merged_samples_chestnut"
    # //////////
    # load dataframe in csv file (all samples interpolated)
    df = pd.read_csv("HPC_features/" + v_merged_samples + "/all_samples_interpolated" + from_raw_data + freq + impute_algo + ".csv")

    df.columns = df.columns.str.replace('tile_o_left',
                                        'tile_o')  # change error in raw data file
    df['date'] = df['date_1']  # for chestnut

    # année bissextiles
    df.columns = df.columns.str.replace('20200331', '20200401')  # change 31/03/2020 to 01/04/2020 to have consistent number of dates in april
    df.columns = df.columns.str.replace('20200430',
                                        '20200501')  # change 31/03/2020 to 01/04/2020 to have consistent number of dates in april

    list_dates = list(set([s[0:8] for s in df.columns.values if '20' in s]))
    list_dates.sort()
    print(list_dates)

    # list_tiles = list(set(df['tile_o']))
    # print(list_tiles)
    # list_tiles.sort()
    # print(list_tiles)

    # ADD new indices (CRre / CRswir)
    dict_indices = {}
    dict_indices['CRre'] = compute_CRre
    dict_indices['CRswir'] = compute_CRswir

    df = add_index_to_df(df,
                         dict_indices)

    dti = pd.to_datetime(pd.Series(list_dates))

    list_columns = df.columns
    list_columns_selected = []

    for band in selected_band:
        for col in list_columns:
            if band in col:
                list_columns_selected.append(col)

    list_columns_selected.sort()
    df_selected = df[list_columns_selected]

    selected_band = ''.join(selected_band)
    selected_dates = "_all_dates_"

    # list_columns = df_selected.columns
    # list_columns_selected = []
    # selected_months = [
    #     # '01',
    #     # '02',
    #     # '03',
    #     # '04',
    #     '05',
    #     '06',
    #     '07',
    #     '08',
    #     '09',
    #     # '10',
    # ]
    # for col in list_columns:
    #     month = col[4:6]
    #     for selected_month in selected_months:
    #         if selected_month == month:
    #             list_columns_selected.append(col)
    #
    # list_columns_selected.sort()
    # df_selected = df_selected[list_columns_selected]
    # df_selected = df_selected[list_columns_selected]
    # list_dates = list(set([s[0:8] for s in df_selected.columns.values if '20' in s]))
    # list_dates.sort()
    # dti = pd.to_datetime(pd.Series(list_dates))
    # selected_dates = "_early_10_"
    # selected_dates = "_summer_"

    df_selected_arr = df_selected.values
    df_selected_arr[np.where(np.isinf(df_selected))] = np.nan
    df_selected = pd.DataFrame(df_selected_arr, columns=df_selected.columns)

    if select_labels:
        idx_other_years = np.where(np.logical_or(df['date'] == 2020, df['date'] == 2021))[0]

        df_selected = df_selected.iloc[idx_other_years].reset_index(drop=True)
        print(df.shape)
        df = df.iloc[idx_other_years].reset_index(drop=True)
        print(df.shape)

    if cloud_mask:
        cloud_mask_str = '_B2_cloud_msk'
        print("filtering clouds")
        for date in list_dates:
            idx_clouds = np.where(df_selected[date + '_B2'] > 450)[0]
            print(date, len(idx_clouds))
            list_feat_date = [feature for feature in df_selected.columns.values if date in feature]
            df_selected.loc[idx_clouds, list_feat_date] = np.nan

    if np.logical_and(N_year_used == 2, start_nov):
        df['date'] = np.where(df['date'] == 2017, 2018, df['date'])
    if N_year_used == 3:
        df['date'] = np.where(df['date'] == 2017, 2018, df['date'] )
    if N_year_used == 4:
        df['date'] = np.where(df['date'] == 2017, 2019, df['date'] )
        df['date'] = np.where(df['date'] == 2018, 2019, df['date'])

    if cloud_mask_str == '_Label_shift_1Y':
        if N_year_used == 2:
            df['date'] = np.where(df['date'] == 2017, 2018, df['date'])
        print(df['date'])
        df['date'] = df['date'] - 1
        print(df['date'])

    if cloud_mask_str == '_Label_shift_2Y':
        df['date'] = np.where(df['date'] == 2017, 2018, df['date'])
        print(df['date'])
        df['date'] = df['date'] - 2
        print(df['date'])

    acq_date_samples = df['date']

    # ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # !! SORTING DATAFRAME USING LABEL YEARS
    df_sort, list_col = sort_df_for_classif(df_selected, N_year_used, acq_date_samples, start_nov=start_nov,
                                            end_early=end_early, month_early=month_early, return_col=True)
    print(list_col)
    # REORDER COLUMNS IN IOTA2 ORDER
    # Order from ACQUISITION 1 to ACQ N
    # BANDS in chronological order B2_ACQ1, B3_ACQ1, ...., B2_ACQN ... B12_ACQN
    # + Additional features in chronological order (CRswir_ACQ1.... CRswir_ACQN, CR_re_ACQU1, ...)
    df_sort = pd.DataFrame(df_sort, columns=list_col)
    list_col_for_iota2 = []
    order_dates = list(set([s[0:8] for s in list_col]))
    order_dates.sort()
    print(order_dates)

    if selected_band == 'CR':
        order_additional_feat = ['CRswir', 'CRre']
        for f in order_additional_feat:
            for date in order_dates:
                list_col_for_iota2.append(date + '_' + f)
    elif selected_band == 'CRswir':
        order_additional_feat = ['CRswir']
        for f in order_additional_feat:
            for date in order_dates:
                list_col_for_iota2.append(date + '_' + f)
    elif selected_band == 'BCR':
        order_bands = ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12']
        for date in order_dates:
            for f in order_bands:
                list_col_for_iota2.append(date + '_' + f)

        order_additional_feat = ['CRswir', 'CRre']
        for f in order_additional_feat:
            for date in order_dates:
                list_col_for_iota2.append(date + '_' + f)

    print(list_col_for_iota2)
    df_sort = df_sort[list_col_for_iota2].values
    # err

    if cloud_mask or impute_feat:
        # df_sort = Norm.inverse_transform(KNNImputer().fit_transform(Norm.fit_transform(df_sort)))
        estimator = ExtraTreesRegressor(n_estimators=5, max_depth=15, n_jobs=-1)
        print("imputing")
        IImp = IterativeImputer(max_iter=5,
                                # estimator=estimator,
                                # n_nearest_features=20,
                                # sample_posterior= True,
                                skip_complete=True,
                                verbose=2,
                                )
        Norm = MinMaxScaler()
        df_sort = Norm.inverse_transform(IImp.fit_transform(Norm.fit_transform(df_sort)))

    df['DEPERIS_pe'] = df['prop_DEPER']
    df_labels = df['DEPERIS_pe']
    # 3 classes
    # df_labels_classif = np.where(df_labels < 0.2, 0, df_labels)
    # df_labels_classif = np.where(np.logical_and(df_labels >= 0.2, df_labels < 0.5), 1, df_labels_classif)
    # df_labels_classif = np.where(df_labels >= 0.5, 2, df_labels_classif)
    # list_classes = ['%D+<20%', "20%=<%D+ <50%", "%D+>=50%"]
    # # plot_ts_3_classes(df_sort, df_labels, dti, title='', y_name=selected_band, save=False, outdir='')
    #
    # # 2 classes
    # # BE SURE THAT NO LABEL == 0, incompatible with iota2 classif !
    # # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # # threshold_deperis = 0.2
    # # df_labels_classif = np.where(df_labels < threshold_deperis, 0, df_labels)
    # # df_labels_classif = np.where(df_labels >= threshold_deperis, 1, df_labels_classif)
    # list_classes = ['%D+<20%', "%D+>=20%"]
    if n_classes == 2:
        # 2 classes
        # BE SURE THAT NO LABEL == 0, incompatible with iota2 classif !
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        threshold_deperis = 0.2
        df_labels_classif = np.where(df_labels < threshold_deperis, 1, df_labels)
        df_labels_classif = np.where(df_labels >= threshold_deperis, 2, df_labels_classif)
        list_classes = ['%D+<20%', "%D+>=20%"]
    else:
        df_labels_classif = np.where(df_labels < 0.2, 1, df_labels)
        df_labels_classif = np.where(np.logical_and(df_labels >= 0.2, df_labels < 0.5), 2, df_labels_classif)
        df_labels_classif = np.where(df_labels >= 0.5, 3, df_labels_classif)
        list_classes = ['%D+<20%', "20%=<%D+ <50%", "%D+>=50%"]


    # /////////////
    # data augmentation
    list_year_test = np.unique(df['date'])
    # list_year_test = [2017, 2018, 2019, 2021]
    i = 0
    for i_date in range(len(list_year_test)):

        df_selected_copy = df_selected.copy()
        df_selected_copy = df_selected_copy.iloc[np.where(df['date'] != list_year_test[i_date])[0], :].reset_index(
            drop=True)
        df_copy = df.copy()
        df_copy = df_copy.iloc[np.where(df_copy['date'] != list_year_test[i_date])[0], :].reset_index(drop=True)
        df_selected_copy['date'] = list_year_test[i_date]
        # df_sort_i = sort_df_for_classif(df_selected_copy, N_year_used, df_selected_copy['date'], start_nov=False)

        # all unlabeled
        # if i == 0:
        #     df_unlabelled = df_sort_i.copy()
        # else:
        #     df_unlabelled = np.concatenate([df_unlabelled, df_sort_i], axis=0)

        # only sure
        dates_sel_plot = df_copy['date'].values
        lab_sel_plot = df_copy['DEPERIS_pe'].values
        df_copy['id'] = df_copy['id_unique']
        df['id'] = df['id_unique']
        id_sel_plot = df_copy['id'].values

        id_year = list(df['id'].values[np.where(df['date'] == list_year_test[i_date])[0]])

        # print(id_year)
        # print(id_year_p)
        # list_match = []
        # for id_temp in id_year:
        #     if id_temp in list(id_sel_plot):
        #         list_match.append(id_temp)
        # print(list_match)

        cond0 = np.ones(len(id_sel_plot), dtype=bool)
        for pl in range(cond0.shape[0]):
            if id_sel_plot[pl] in id_year:
                cond0[pl] = False

        cond1 = np.logical_and(lab_sel_plot < 0.2,
                               np.logical_or(dates_sel_plot == list_year_test[i_date] + 1,
                                             dates_sel_plot == list_year_test[i_date] + 2))
        # cond1 = np.zeros(lab_sel_plot.shape[0], dtype=bool)
        #
        # cond1 = np.logical_and(lab_sel_plot < 0.1,
        #                        np.logical_or(dates_sel_plot == list_year_test[i_date] + 1,
        #                                      np.logical_or(dates_sel_plot == list_year_test[i_date] + 2,
        #                                                    dates_sel_plot == list_year_test[i_date] + 3)
        #                                      )
        #                                      )

        # cond1 = np.logical_and(lab_sel_plot < 0.1,
        #                        dates_sel_plot == list_year_test[i_date] + 1)
        print(cond1)
        print(cond1.shape)
        cond2 = np.logical_and(lab_sel_plot > 2, # here we don't add any sample for chestnut
                               np.logical_or(dates_sel_plot == list_year_test[i_date] - 1,
                                             dates_sel_plot == list_year_test[i_date] - 2))
        # cond2 = np.logical_and(lab_sel_plot > 0.2,
        #                        np.logical_or(dates_sel_plot == list_year_test[i_date] - 1,
        #                                      np.logical_or(dates_sel_plot == list_year_test[i_date] - 2,
        #                                                    dates_sel_plot == list_year_test[i_date] - 3)
        #                                      )
        #                                      )

        # cond3 = np.logical_and(lab_sel_plot > 0.3,
        #                        np.logical_or(dates_sel_plot == list_year_test[i_date] + 1,
        #                                      dates_sel_plot == list_year_test[i_date] + 1))
        # #
        # cond4 = np.logical_and(lab_sel_plot < 0.05,
        #                        np.logical_or(dates_sel_plot == list_year_test[i_date] - 1,
        #                                      dates_sel_plot == list_year_test[i_date] - 1))
        #
        # cond3 = np.logical_or(cond3, cond4)
        # cond2 = np.logical_or(cond2, cond3)

        idx_sel = np.where(np.logical_and(cond0,
                                          np.logical_or(cond1, cond2))
                           )[0]
        print(idx_sel.shape)
        # print(id_sel_plot[idx_sel])
        # err
        # idx_sel = np.where(np.logical_or(cond1,
        #                                  np.logical_or(cond2,
        #                                                cond3)))[0]

        # lab_sel_plot[idx_sel] = np.where(dates_sel_plot == list_year_test[i_date] - 2, lab_sel_plot[idx_sel] - 0.1, lab_sel_plot[idx_sel])
        # lab_sel_plot[idx_sel] = np.where(dates_sel_plot[idx_sel] == list_year_test[i_date] - 2, lab_sel_plot[idx_sel] + 0.2, lab_sel_plot[idx_sel])
        if len(idx_sel) == 0:
            continue
        else:
            df_sel_final = pd.DataFrame(df_selected_copy.iloc[idx_sel, :].values, columns=df_selected_copy.columns)
            df_date_final = pd.DataFrame(df_selected_copy['date'].iloc[idx_sel].values, columns=['date'])
            df_sort_sure_i = sort_df_for_classif(df_sel_final,
                                                 n_year=N_year_used,
                                                 acq_date=df_date_final,
                                                 start_nov=start_nov,
                                                 end_early=end_early,
                                                 month_early=month_early
                                                 )
            if i == 0:
                df_sure = df_sort_sure_i.copy()
                year_df_sure = df_selected_copy['date'].values[idx_sel]
                y_df_sure = lab_sel_plot[idx_sel]
                id_df_sure = df_copy["id_unique"].values[idx_sel]
            else:
                df_sure = np.concatenate([df_sure, df_sort_sure_i], axis=0)
                year_df_sure = np.concatenate([year_df_sure, df_selected_copy['date'].values[idx_sel]])
                y_df_sure = np.concatenate([y_df_sure, lab_sel_plot[idx_sel]])
                id_df_sure = np.concatenate([id_df_sure, df_copy["id_unique"].values[idx_sel]])

            i += 1

    y_df_sure_deperis = y_df_sure.copy()

    if n_classes == 3:
        # 3 classes
        y_df_sure_deperis = np.where(y_df_sure < 0.2, 1, y_df_sure_deperis)
        y_df_sure_deperis = np.where(np.logical_and(y_df_sure >= 0.2, y_df_sure < 0.5), 2, y_df_sure_deperis)
        y_df_sure_deperis = np.where(y_df_sure >= 0.5, 3, y_df_sure_deperis)
    elif n_classes == 2:
        y_df_sure = np.where(y_df_sure < 0.2, 0, 1)
    else:
        raise ValueError('Only 2 or 3 classes accepted')

    y_df_sure = y_df_sure_deperis

    # REORDER COLUMNS IN IOTA2 ORDER
    df_sure = pd.DataFrame(df_sure, columns=list_col)
    df_sure = df_sure[list_col_for_iota2].values
    print("DF SURE", df_sure.shape)
    print(np.unique(year_df_sure, return_counts=True))
    print(np.unique(y_df_sure, return_counts=True))

    # //////////////
    # Begining preparation for the CV

    # to have various submodels based on a specific attribute (tile, ser; etc.)
    list_submodels_classes = df['tile_o'].values
    list_submodels_classes_placette = []

    #
    placette_array = df['id_unique'].values
    list_DEPERIS_placette = []
    list_DEPERIS_score_samples = []

    list_tile_placette = []
    list_ser_placette = []
    list_DEPERIS_samples = []

    list_df_placette = []
    list_id_placette = []
    list_placette_idx = []
    list_date_samples = []

    dict_placette_label = {}

    i = 0
    for placette in np.unique(df["id_unique"]):
        idx_placette = np.where(placette_array == placette)[0]
        df_placette = df_sort[idx_placette, :]

        list_DEPERIS_placette.append(df_labels_classif[idx_placette[0]])
        list_df_placette.append(df_placette)

        list_tile_placette.append(df['tile_o'][idx_placette])
        list_date_samples.append(df['date'].values[idx_placette])
        list_submodels_classes_placette.append(list_submodels_classes[idx_placette])

        list_DEPERIS_samples.append(df_labels_classif[idx_placette])
        list_DEPERIS_score_samples.append(df_labels[idx_placette])
        list_id_placette.append(placette*np.ones(len(idx_placette)))

        dict_placette_label[placette] = df_labels_classif[idx_placette[0]]
        list_placette_idx.append(idx_placette)
        i += 1

    skf = StratifiedKFold(n_splits=5, shuffle=True, random_state=13)
    skf.get_n_splits(list_DEPERIS_placette, list_DEPERIS_placette)

    skf3 = StratifiedKFold(n_splits=3, shuffle=True, random_state=13)
    skf3.get_n_splits(list_DEPERIS_placette, list_DEPERIS_placette)

    list_all_ypred = []
    list_all_ytest = []

    list_het_plac = []

    list_all_ypred_placette = []
    list_all_ytest_placette = []

    y_probas_all = np.zeros(df_sort.shape[0])
    y_pred_all = np.zeros(df_sort.shape[0])

    exp = 0
    list_recall_runs = []
    list_precision_runs = []

    # First we run a CV to validate our model
    for train_index, test_index in skf.split(list_DEPERIS_placette, list_DEPERIS_placette):

        split_val = False
        if split_val:
            print(np.asarray(list_DEPERIS_placette))

            train_index, val_index = train_test_split(train_index, test_size=0.1, shuffle=True,
                                                      stratify=np.asarray(list_DEPERIS_placette)[train_index])
            list_Xval = [list_df_placette[index] for index in list(val_index)]
            X_validation = np.concatenate(list_Xval, axis=0)
            print(X_validation.shape)
            list_yval = [list_DEPERIS_samples[index] for index in list(val_index)]
            y_validation = np.concatenate(list_yval, axis=0)

        print("EXP #", exp)
        # Stack Xtrain / ytrain
        list_Xtrain = [list_df_placette[index] for index in list(train_index)]
        X_train = np.concatenate(list_Xtrain, axis=0)
        print(X_train.shape)
        list_ytrain = [list_DEPERIS_samples[index] for index in list(train_index)]
        y_train = np.concatenate(list_ytrain, axis=0)

        # Stack Xtest / ytest
        list_Xtest = [list_df_placette[index] for index in list(test_index)]
        X_test = np.concatenate(list_Xtest, axis=0)

        list_ytest = [list_DEPERIS_samples[index] for index in list(test_index)]
        y_test = np.concatenate(list_ytest, axis=0)
        print(y_test)
        print(y_test.shape)

        list_score_ytest = [list_DEPERIS_score_samples[index] for index in list(test_index)]
        y_score_test = np.concatenate(list_score_ytest, axis=0)

        list_all_ytest.append(y_test.copy())

        # get id samples & score placette
        list_id_test = [list_id_placette[index] for index in list(test_index)]
        list_id_test = np.concatenate(list_id_test, axis=0)
        print(list_id_test.shape)

        list_class_test = [list_DEPERIS_placette[index] for index in list(test_index)]
        list_class_test = np.asarray(list_class_test)
        print(list_class_test.shape)

        # dates
        list_date_test = [list_date_samples[index] for index in list(test_index)]
        list_date_test = np.concatenate(list_date_test, axis=0)

        list_date_train = [list_date_samples[index] for index in list(train_index)]
        list_date_train = np.concatenate(list_date_train, axis=0)


        list_test_idx = [list_placette_idx[index] for index in list(test_index)]
        list_test_idx = np.concatenate(list_test_idx, axis=0)

        forest = RandomForestClassifier(n_estimators=1000,
                                        max_depth=15,
                                        # max_depth=10,
                                        # min_samples_leaf=3,
                                        n_jobs=-1,
                                        # max_features='log2',
                                        # bootstrap=False,
                                        class_weight='balanced',
                                        # class_weight={0: 1,
                                        #               1: 1.7}
                                        )

        clf = forest
        #
        # # /////////////////////
        # # NORMAL PRED
        # clf.fit(X=X_train, y=y_train,
        #         )
        # y_pred = clf.predict(X_test)
        # y_proba = clf.predict_proba(X_test)
        # print("ALL PIXELS")
        # conf_mat = confusion_matrix(y_test, y_pred)
        # print(conf_mat)
        # prec = conf_mat[1, 1] / np.sum(conf_mat[:, 1])
        # rec = conf_mat[1, 1] / np.sum(conf_mat[1, :])
        #
        # print("prec, rec, F1")
        # print(prec, rec, 2*prec*rec/(prec+rec))
        #
        # conf_mat = confusion_matrix(y_test, y_pred, normalize='true')
        # print(conf_mat)

        # FOR IOTA2 WE WILL TRAIN A SHARK RF CLASSIFIER INSTEAD OF SKLEARN RF:
        samples_file = "gpd_temp_train" + str(exp) + selected_band + selected_dates + str(n_classes) + 'classes' + str_nov + ".sqlite"

        classifier_options = {
            "classifier.sharkrf.nbtrees": 1000,  # number of trees
            "classifier.sharkrf.nodesize": 15,  # min samples in node to accept splits
            # "classifier.sharkrf.oobr": 0.5,
        }

        if use_SMOTE and not data_augmentation:
            X_train, y_train = SMOTE(n_jobs=-1).fit_resample(X_train, y_train)

        if data_augmentation:
            if selected_band == 'CR':
                order_additional_feat = ['CRswir', 'CRre']
            elif selected_band == 'CRswir':
                order_additional_feat = ['CRswir']
            else:
                raise ValueError("not implemented for BCR")

            X_train = np.concatenate([X_train, df_sure], axis=0)
            y_train = np.concatenate([y_train, y_df_sure])

            if balance_each_year:
                list_date_train = np.concatenate([list_date_train, year_df_sure])
                iiter = 0
                for iyear in np.unique(list_date_train):

                    idx_year = np.where(list_date_train == iyear)[0]

                    X_train_year = X_train[idx_year, :]
                    y_train_year = y_train[idx_year]

                    X_train_year, y_train_year = SMOTE(n_jobs=-1).fit_resample(X_train_year, y_train_year)

                    if iiter == 0:
                        X_train_final = X_train_year
                        y_train_final = y_train_year
                    else:
                        X_train_final = np.concatenate([X_train_final, X_train_year], axis=0)
                        y_train_final = np.concatenate([y_train_final, y_train_year])

                    iiter += 1

                X_train = X_train_final
                y_train = y_train_final

            else:
                X_train, y_train = SMOTE(n_jobs=-1).fit_resample(X_train, y_train)


        print('Training', X_train.shape)
        train_shark_rf(data=X_train, labels_classif=y_train, samples_file=samples_file,
                       classifier_options=classifier_options,
                       list_col_for_iota2=list_col_for_iota2, output_model='tmp_otb/model.txt')

        samples_file = "/media/florian/data/tmp/gpd_temp_test" + str(exp) + selected_band + selected_dates + str(n_classes) + 'classes' + str_nov + ".sqlite"
        y_pred, y_proba = predict_shark_rf(data=X_test, labels_classif=y_test, samples_file=samples_file,
                                           list_col_for_iota2=list_col_for_iota2, output_model='tmp_otb/model.txt')
        list_all_ypred.append(y_pred)
        y_probas_all[list_test_idx] = y_proba
        y_pred_all[list_test_idx] = y_pred

        print("ALL PIXELS")
        conf_mat = confusion_matrix(y_test, y_pred)
        print(conf_mat)
        prec = conf_mat[1, 1] / np.sum(conf_mat[:, 1])
        rec = conf_mat[1, 1] / np.sum(conf_mat[1, :])
        print("prec, rec, F1")
        print(prec, rec, 2 * prec * rec / (prec + rec))

        conf_mat = confusion_matrix(y_test, y_pred, normalize='true')
        print(conf_mat)

        print("PLOT LEVEL")
        list_true_label_placette = []
        list_pred_label_placette = []
        for placette in np.unique(list_id_test):
            idx_placette = np.where(np.asarray(list_id_test) == placette)[0]

            y_pred_placette = y_pred[idx_placette]
            pred_placette = mode(y_pred_placette)[0]

            # y_score_placette = y_scores[idx_placette]

            # y_score_sum = np.sum(y_score_placette, axis=0)
            # pred_placette = np.argmax(y_score_sum)

            list_pred_label_placette.append(pred_placette)
            list_true_label_placette.append(dict_placette_label[placette])

            list_het_plac.append(len(np.where(y_pred_placette != pred_placette)[0]) / pred_placette.shape[0])

        list_all_ypred_placette.append(list_pred_label_placette)
        list_all_ytest_placette.append(list_true_label_placette)

        conf_mat = confusion_matrix(list_true_label_placette, list_pred_label_placette)
        print(conf_mat)
        list_precision_runs.append(conf_mat[1, 1] / np.sum(conf_mat[:, 1]))
        list_recall_runs.append(conf_mat[1, 1] / np.sum(conf_mat[1, :]))

        conf_mat = confusion_matrix(list_true_label_placette, list_pred_label_placette, normalize='true')
        print(conf_mat)
        exp += 1

    # print Precision / recall all
    print("Precision", np.mean(list_precision_runs), np.std(list_precision_runs))
    print("Recall", np.mean(list_recall_runs), np.std(list_recall_runs))

    print("FINAL ####")
    list_all_ypred = np.concatenate(list_all_ypred)
    list_all_ytest = np.concatenate(list_all_ytest)

    list_all_het = np.asarray(list_het_plac)
    print("# HET. PLOTS")
    print('mean', np.mean(list_all_het))
    print('std', np.std(list_all_het))
    print('median', np.median(list_all_het))
    print('IQR', np.percentile(list_all_het, 75) - np.percentile(list_all_het, 25))
    # fig, ax = plt.subplots()
    # plt.hist(list_all_het, 50, density=False, facecolor='g', alpha=0.75)
    # plt.show()

    conf_mat = confusion_matrix(list_all_ytest, list_all_ypred)
    print(conf_mat)
    print(conf_mat)
    if conf_mat.shape[0] == 3:
        print('precision 1', conf_mat[0, 0] / np.sum(conf_mat[:, 0]))
        pr1 = conf_mat[0, 0] / np.sum(conf_mat[:, 0])
        print('precision 2', conf_mat[1, 1] / np.sum(conf_mat[:, 1]))
        pr2 = conf_mat[1, 1] / np.sum(conf_mat[:, 1])
        print('precision 3', conf_mat[2, 2] / np.sum(conf_mat[:, 2]))
        pr3 = conf_mat[2, 2] / np.sum(conf_mat[:, 2])
        list_pr = [pr1, pr2, pr3]

    elif conf_mat.shape[0] == 2:
        print('precision 1', conf_mat[0, 0] / np.sum(conf_mat[:, 0]))
        print('precision 2', conf_mat[1, 1] / np.sum(conf_mat[:, 1]))
        pr1 = conf_mat[0, 0] / np.sum(conf_mat[:, 0])
        pr2 = conf_mat[1, 1] / np.sum(conf_mat[:, 1])
        list_pr = [pr1, pr2]

    list_pr_str = ''
    print(list_pr)
    i = 0
    for pr in list_pr:
        list_pr_str += 'Pr' + str(i) + ':' + "{:.2f}".format(list_pr[i]) + ', '
        i += 1

    conf_mat = confusion_matrix(list_all_ytest, list_all_ypred, normalize='true')
    print(conf_mat)

    # Plot non-normalized confusion matrix
    titles_options = [
        ("Confusion matrix (pixel level) \n Features: " + selected_band + ' - Date: ' + selected_dates + ' - Nyear: ' + str(N_year_used) + '\n ' + list_pr_str,
         None),
        ("Normalized confusion matrix (pixel level) \n Features: " + selected_band + ' - Date: ' + selected_dates + ' - Nyear: ' + str(N_year_used) + '\n ' + list_pr_str,
         "true"),
    ]
    for title, normalize in titles_options:
        disp = ConfusionMatrixDisplay.from_predictions(
            y_true=list_all_ytest,
            y_pred=list_all_ypred,
            display_labels=list_classes,
            cmap=plt.cm.Blues,
            normalize=normalize,
            colorbar=False,
        )
        disp.ax_.set_title(title)

        print(title)
        print(disp.confusion_matrix)

    plt.show()

    list_all_ytest_placette = np.concatenate(list_all_ytest_placette)
    list_all_ypred_placette = np.concatenate(list_all_ypred_placette)

    conf_mat = confusion_matrix(list_all_ytest_placette, list_all_ypred_placette)
    print(conf_mat)
    if conf_mat.shape[0] == 3:
        print('precision 1', conf_mat[0, 0] / np.sum(conf_mat[:, 0]))
        pr1 = conf_mat[0, 0] / np.sum(conf_mat[:, 0])
        print('precision 2', conf_mat[1, 1] / np.sum(conf_mat[:, 1]))
        pr2 = conf_mat[1, 1] / np.sum(conf_mat[:, 1])
        print('precision 3', conf_mat[2, 2] / np.sum(conf_mat[:, 2]))
        pr3 = conf_mat[2, 2] / np.sum(conf_mat[:, 2])
        list_pr = [pr1, pr2, pr3]

    elif conf_mat.shape[0] == 2:
        print('precision 1', conf_mat[0, 0] / np.sum(conf_mat[:, 0]))
        print('precision 2', conf_mat[1, 1] / np.sum(conf_mat[:, 1]))
        pr1 = conf_mat[0, 0] / np.sum(conf_mat[:, 0])
        pr2 = conf_mat[1, 1] / np.sum(conf_mat[:, 1])
        list_pr = [pr1, pr2]

    conf_mat = confusion_matrix(list_all_ytest_placette, list_all_ypred_placette, normalize='true')
    print(conf_mat)

    list_pr_str = ''

    i = 0
    for pr in list_pr:
        list_pr_str += 'Pr' + str(i) + ':' + "{:.2f}".format(list_pr[i]) + ', '
        i+=1

    # Plot non-normalized confusion matrix
    titles_options = [
        ("Confusion matrix (plot level) \n Features: " + selected_band + ' - Date: ' + selected_dates + ' - Nyear: ' + str(N_year_used) + '\n ' + list_pr_str, None),
        ("Normalized confusion matrix (plot level) \n Features: " + selected_band + ' - Date: ' + selected_dates + ' - Nyear: ' + str(N_year_used) + '\n ' + list_pr_str, "true"),
    ]
    for title, normalize in titles_options:
        disp = ConfusionMatrixDisplay.from_predictions(
            y_true=list_all_ytest_placette,
            y_pred=list_all_ypred_placette,
            display_labels=list_classes,
            cmap=plt.cm.Blues,
            normalize=normalize,
            colorbar=False,
        )
        disp.ax_.set_title(title)

        print(title)
        print(disp.confusion_matrix)

    plt.show()

    print("######## FINAL TRAINING AND MODEL SAVE")
    # clf.fit(df_sort, df_labels_classif)
    # pred_final = clf.predict(df_sort)

    if n_classes == 2:
        # 2 classes
        # BE SURE THAT NO LABEL == 0, incompatible with iota2 classif !
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        threshold_deperis = 0.2
        df_labels_classif = np.where(df_labels < threshold_deperis, 1, df_labels)
        df_labels_classif = np.where(df_labels >= threshold_deperis, 2, df_labels_classif)
        list_classes = ['%D+<20%', "%D+>=20%"]
    else:
        df_labels_classif = np.where(df_labels < 0.2, 1, df_labels)
        df_labels_classif = np.where(np.logical_and(df_labels >= 0.2, df_labels < 0.5), 2, df_labels_classif)
        df_labels_classif = np.where(df_labels >= 0.5, 3, df_labels_classif)
        list_classes = ['%D+<20%', "20%=<%D+ <50%", "%D+>=50%"]

    cloud_mask_str += str(n_classes) + "classes"

    samples_file = "/media/florian/data/tmp/final_data.sqlite"
    try:
        os.remove(samples_file)  # to avoid any problem we delete any temporary file
    except:
        print('no temp final file')
    out_model = "model_chestnut/" + selected_band + selected_dates + str(N_year_used) + "year_used_interpol_" + from_raw_data + freq + impute_algo + cloud_mask_str + '.txt'

    if use_SMOTE and not data_augmentation:
        df_sort, df_labels_classif = SMOTE(n_jobs=-1).fit_resample(df_sort, df_labels_classif)

    if data_augmentation:
        if selected_band == 'CR':
            order_additional_feat = ['CRswir', 'CRre']
        elif selected_band == 'CRswir':
            order_additional_feat = ['CRswir']
        else:
            raise ValueError("not implemented for BCR")

        X_train = np.concatenate([df_sort, df_sure], axis=0)
        y_train = np.concatenate([df_labels_classif, y_df_sure])

        if balance_each_year:
            list_date_train = np.concatenate([df['date'].values, year_df_sure])
            iiter = 0
            for iyear in np.unique(list_date_train):

                idx_year = np.where(list_date_train == iyear)[0]

                X_train_year = X_train[idx_year, :]
                y_train_year = y_train[idx_year]

                X_train_year, y_train_year = SMOTE(n_jobs=-1).fit_resample(X_train_year, y_train_year)

                if iiter == 0:
                    X_train_final = X_train_year
                    y_train_final = y_train_year
                else:
                    X_train_final = np.concatenate([X_train_final, X_train_year], axis=0)
                    y_train_final = np.concatenate([y_train_final, y_train_year])

                iiter += 1

            X_train = X_train_final
            y_train = y_train_final

        else:
            X_train, y_train = SMOTE(n_jobs=-1).fit_resample(X_train, y_train)

        df_sort = X_train
        df_labels_classif = y_train

    train_shark_rf(data=df_sort, labels_classif=df_labels_classif, samples_file=samples_file,
                   classifier_options=classifier_options,
                   list_col_for_iota2=list_col_for_iota2, output_model=out_model)

    pred_final, y_proba = predict_shark_rf(data=df_sort, labels_classif=df_labels_classif, samples_file=samples_file,
                                           list_col_for_iota2=list_col_for_iota2, output_model=out_model)

    conf_mat = confusion_matrix(df_labels_classif, pred_final, normalize='true')
    print(conf_mat)
    # model_file = open("model/" + selected_band + selected_dates + str(N_year_used) + "year_used_interpol_" + from_raw_data + freq + impute_algo + cloud_mask_str + '.txt', "wb")
    # pickle.dump((clf, None), model_file)
    # model_file.close()

    idx_tile = np.where(np.logical_and(df['tile_o'] == 'T31UDP',
                                       df['date'] == 2021))[0]

    conf_mat = confusion_matrix(df_labels_classif[idx_tile], pred_final[idx_tile], normalize='true')
    print(conf_mat)

