from sklearn.experimental import enable_iterative_imputer  # noqa
from train_new_model.utils.utils_ts_analysis import *
from train_new_model.utils.utils_compute_indices import *
import argparse


def reinterpolate_df(df, out, freq='10D', limit=20, method='time'):
    list_S2_data = [col for col in df.columns if '20' in col]
    list_S2_data.sort()

    list_other_col = [col for col in df.columns if '20' not in col]

    df_base = df[list_other_col]

    list_dates = list(set([s[0:8] for s in list_S2_data if '20' in s]))
    list_dates.sort()

    dti = pd.to_datetime(pd.Series(list_dates))

    list_bands = ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12']
    # list_years = [2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023]
    list_years = list(set([s[0:4] for s in list_S2_data if '20' in s]))
    list_years.sort()

    df_S2 = df[list_S2_data]
    # freq = '20D'

    for band in list_bands:
        print(band)
        if band != 'B8':
            selected_bands = [col for col in df_S2.columns if band in col]
        else:
            selected_bands = [col for col in df_S2.columns if (band in col and '_B8A' not in col)]

        df_band = df_S2[selected_bands]

        for year in list_years:  # we do interpolation by year to have the same number of columns for each year !
            list_new_dates = pd.date_range(start=str(year) + "0101", end=str(year) + "1231", freq=freq)
            interpolated_df = np.zeros([df_band.shape[0], len(list_new_dates)])

            for sample in range(df_band.shape[0]):
                # print(dti)
                # print(df_band.values[sample, :])
                TS = pd.Series(df_band.values[sample, :], index=dti)

                # if year == 2016:
                #     plt.plot(TS, marker='x')
                #     plt.show()
                interp_TS = TS.asfreq("D")
                interp_TS = interp_TS.interpolate(method=method,
                                                  limit=limit,
                                                  limit_direction='both')
                # if year == 2016:
                #     plt.plot(interp_TS)
                interp_TS = interp_TS[interp_TS.index.year == year]
                interp_TS = interp_TS.asfreq(freq)
                # if year == 2016:
                #     plt.plot(interp_TS, marker='.')
                interpolated_df[sample, :] = interp_TS.values[0:len(list_new_dates)]
                # if year == 2016:
                #     plt.show()
                # for i in range(len(interp_TS.index)):
                #     print(interp_TS.index[i])
            list_new_dates = interp_TS.index[0:len(list_new_dates)].strftime("%Y%m%d").tolist()
            list_new_name = []
            for date in list_new_dates:
                list_new_name.append(date + '_' + band)

            df_base[list_new_name] = interpolated_df

    print(df_base)
    df_base.to_csv(out, index=False)

    return df_base


if __name__ == "__main__":

    # output_folder = "merged_samples_pine"  # if changing / updating the features...
    parser = argparse.ArgumentParser(description="Interpolate IOTA2 extracted files")
    parser.add_argument("--output_folder", type=str,
                        default="HPC_features/extracted_samples/",
                        help="Path to the output folder")
    args = parser.parse_args()

    output_folder = args.output_folder

    df = pd.read_csv(output_folder + "/all_samples.csv")

    # df = pd.read_csv("HPC_features/" + output_folder + "/Samples_label_2021.csv")
    freq = '10D'
    date_range = '_0101_1231'
    reinterpolate_df(df, out=output_folder + "/all_samples_interpolated" + freq + date_range + ".csv", freq=freq)


