from utils.generate_cfg_file import generate_cfg as generate_cfg_part1
import os
import shutil
import iota2.Iota2 as Iota2
import subprocess
from utils.utils import load_config_variable
import argparse
from argparse import RawTextHelpFormatter

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="This routine extract S2 time series based on yearly shapefiles using a config .cfg file provided by the user",
        formatter_class=RawTextHelpFormatter
    )
    parser.add_argument(
        "-config_file",
        dest="config_file",
        help=
        "Config file with mandatory variables \n"
        "EXAMPLE OF CFG FILE: \n \n"
        "label = 'oak'  # name used to identify your results"
        "name_task = 'extract_oak'  # name folder to extract files" 
        "S2_path = '/work/RECONFORT/iota2_project_reconfort/s2_data_annual'  # path to S2 data, sorteb by tile"
        "number_of_chunks = '100'  # images are processed in small chunk to save RAM"
        "list_tiles = 'T31UDQ T31UDP T31UCQ T31UCP T31TEN T31TEM T31TDN T31TDM T31TCN T31TCM T30TYT'  # eg, 'T31UDP T31TDN T30TYT'"
        "scheduler_type = 'Slurm'"
        "nb_parallel_tasks = '10'"
        "ground_truth_path = '/work/RECONFORT/iota2_project_reconfort/vector_db/chene_fusion_buf_WGS_2154_LAMB93_without_roads_with_month_'  # will be changed using the label year"
        "list_S2_years = '2022 2023'  # careful, we split using white spaces, i.e., '2022 2023' will become ['2022', '2023']"
        "list_label_years = '2017 2018 2019 2020 2021 2022'  # careful, we split using white spaces, i.e., '2022 2023' will become ['2022', '2023']"
        "out_dir_root_work_iota2 = '/work/scratch/data/mouretf/'"
        "out_dir_root_save_feat = '/work/scratch/data/mouretf/extracted_samples/'"
        ,
        required=True
    )
    args = parser.parse_args()
    config_file = args.config_file

    new_root = os.path.dirname(os.path.abspath(__file__))
    os.chdir(new_root)
    print('root of the project', new_root)

    # SOME INTERNAL VARIABLES
    # we used some random points, 1 of each class per tile, to be able to use iota2.
    # ground_truth_path = 'iota2/vector_db/random_points.shp'
    # ///

    # SOME USER VARIABLES - see config_file.cfg
    dict_config = load_config_variable(config_file)
    print('list of user variables')
    print(dict_config)

    v_model = "v3"
    S2_path = dict_config["S2_path"]
    list_tiles = dict_config['list_tiles']
    ground_truth_path = dict_config["ground_truth_path"]
    out_dir_root = dict_config["out_dir_root_work_iota2"]
    out_dir_save = dict_config["out_dir_root_save_feat"]
    list_S2_years = dict_config['list_S2_years'].split()
    list_label_years = dict_config['list_label_years'].split()

    # add quote to avoid problems with iota2
    if list_tiles[0] != "'":
        list_tiles = "'" + list_tiles + "'"

    n_chunks = dict_config['number_of_chunks']

    try:
        scheduler_type = dict_config['scheduler_type']
    except:
        scheduler_type = 'localCluster'

    try:
        nb_parallel_tasks = dict_config['nb_parallel_tasks']
    except:
        nb_parallel_tasks = '1'

    # label_year = dict_config['label']  # not used, only for generating the cfg files
    name_task = dict_config['name_task']

    for label_year in list_label_years:
        # /////////////////////////////////
        # IOTA2 FOR LEARNING SAMPLES
        for S2_year in list_S2_years:
            out_dir_files = out_dir_root + name_task + '/' + label_year + '/' + S2_year
            if not os.path.exists(out_dir_files):
                os.makedirs(out_dir_files)
            # for S2_year in ["2016"]:
            # PART 1 CFG
            generate_cfg_part1(
                label_year,
                S2_year,
                out_dir_cfg='generated_config_files/',
                out_dir_files=out_dir_files,
                S2_path=S2_path,
                ground_truth_path=ground_truth_path + label_year + '.shp',
                list_tiles=list_tiles,
                number_of_chunks=n_chunks,
                v_model=v_model
            )

            # run iota2 part 1
            command_path = shutil.which('Iota2.py')
            print("command")
            print(command_path)
            subprocess.run([
                "python", command_path,
                '-config', 'generated_config_files/config_Labels_' + label_year + '_year_' + S2_year + '.cfg',
                '-config_ressources', 'iota2/config/iota2_resources.cfg',
                '-nb_parallel_tasks', nb_parallel_tasks,
                '-restart',
                '-scheduler_type', scheduler_type
            ])

            path_samples = out_dir_files + name_task + '/' + label_year + '/' + S2_year + '/learningSamples/'
            out_dir_feat = out_dir_save + name_task + '/'  + label_year + '/' + S2_year + '/'

            try:
                shutil.copy(
                    path_samples,
                    out_dir_files
                )
            except:
                'wrong path'
