from train_new_model.utils.utils_ts_analysis import *
import glob
import argparse
import geopandas as gpd


def merge_sqlites(dir_shapefile, gf_data_root, end_shp_file, output_folder):
    list_gdf = []
    list_label = glob.glob(gf_data_root + '/*/')

    for label_set in list_label:
        print(label_set)
        label_year = label_set[-1 - 4: -1]

        list_gdf_label = []
        list_S2_years = glob.glob(label_set + '/*/')
        list_S2_years.sort()
        print(list_S2_years)
        print("Merging years")
        i_year = 0
        for S2_year in list_S2_years:
            print(S2_year)

            if i_year == 0:
                gdf_label_set = gpd.read_file(S2_year + 'learningSamples/Samples_region_1_seed0_learn.sqlite')
                # print(gdf_label_set.columns)
                try:
                    gdf_label_set = gdf_label_set.drop(['i2label', 'region', 'deperis_cl', 'originfid'], axis=1)
                except:
                    gdf_label_set = gdf_label_set.drop(['i2label', 'region', 'dep_cor', 'originfid'], axis=1)
            else:
                gdf_temp = gpd.read_file(S2_year + 'learningSamples/Samples_region_1_seed0_learn.sqlite')
                try:
                    gdf_temp = gdf_temp.drop(['i2label', 'region', 'deperis_cl', 'originfid', 'tile_o'], axis=1)
                except:
                    gdf_temp = gdf_temp.drop(['i2label', 'region', 'dep_cor', 'originfid', 'tile_o'], axis=1)
                # print(gdf_temp.shape)
                gdf_label_set = gpd.sjoin(gdf_label_set, gdf_temp, how='right', predicate='intersects')
                gdf_label_set = gdf_label_set.drop(['index_left'], axis=1)
            # print(gdf_label_set['id_unique'])
            # print(gdf_label_set)
            i_year += 1

        # shp_labels = gpd.read_file(dir_shapefile + label_year + end_shp_file + '.shp')
        shp_labels = gpd.read_file(dir_shapefile + end_shp_file + label_year + '.shp')

        list_col_iota2 = gdf_label_set.columns.values.tolist()
        list_col_shp = shp_labels.columns.values.tolist()

        gdf_label_set = gpd.sjoin(gdf_label_set, shp_labels, how='left', predicate='within')

        print(list_col_iota2)
        print(list_col_shp)
        # gdf_label_set = gdf_label_set[list_col_shp[0:-2] + list_col_iota2]
        gdf_label_set = gdf_label_set[list_col_shp[0:-1] + list_col_iota2]

        # rename column name from iota2 to DATE + BANDE only - shorter (adapted to shp format)
        gdf_label_set.rename(columns=convert_iota2_names, inplace=True)
        list_gdf.append(gdf_label_set.copy())
        print(gdf_label_set.shape)
        gdf_label_set.drop('geometry', axis=1).to_csv(
            output_folder + '/Samples_label_' + label_year + '.csv',
            index=False)

    print(list_gdf)
    gdf_all = pd.concat(list_gdf).reset_index(drop=True)
    if 'date_1' in gdf_all.columns.values:
        gdf_all['date'] = gdf_all['date_1']  # for chestnut

    if 'id_unique' not in gdf_all.columns.values:
        def assign_unique_id(list_of_strings):
            unique_ids = {}
            id = 0
            for string in list_of_strings:
                if string not in unique_ids:
                    unique_ids[string] = id
                    id += 1
            return [unique_ids[string] for string in list_of_strings]

        id = list(gdf_all['id'].values)
        uniqueids = assign_unique_id(id)
        gdf_all['id_unique'] = np.asarray(uniqueids).astype(str)

    gdf_all['id_unique'] = gdf_all['id_unique'].astype(str) + gdf_all['date'].astype(str)
    gdf_all['id_unique_pixel'] = gdf_all['id_unique'].astype(float) + np.linspace(0, gdf_all.shape[0], gdf_all.shape[0])

    # gdf_geom = gdf_all[['id', 'id_unique', 'id_unique_pixel', 'geometry']]
    gdf_geom = gdf_all[['id_unique', 'id_unique_pixel', 'geometry']]
    gdf_geom.to_file(output_folder + '/geom_all_samples.gpkg', index=False, driver='GPKG')
    print(gdf_all.shape)
    gdf_all.drop('geometry', axis=1).to_csv(output_folder + '/all_samples.csv', index=False)

if __name__ == "__main__":

    '''
    This routine aims at merging sqlite files extracted with iota2 into a unique shp file with labels.
    '''
    # dir_shapefile = '/work/RECONFORT/reconfort_package/reconfort_learning/iota2/vector_db_pine/'
    # gf_data_root = "HPC_features/extracted_samples_pine/"
    #
    # end_shp_file = '_final_simple'  # for chestnut
    # end_shp_file = 'pine_all_buf_no_road_simple_'  # for pine
    #
    # output_folder = 'merged_samples_pine'

    parser = argparse.ArgumentParser(description="Merge IOTA2 extracted files")
    parser.add_argument("--dir_shapefile", type=str, default="/work/RECONFORT/reconfort_package/reconfort_learning/iota2/vector_db_pine/",
                        help="Path to the directory containing the shapefiles. (default: /work/RECONFORT/reconfort_package/reconfort_learning/iota2/vector_db_pine/)")
    parser.add_argument("--gf_data_root", type=str, default="HPC_features/extracted_samples/",
                        help="Path to the root directory of the extracted samples. (default: HPC_features/extracted_samples/)")
    parser.add_argument("--end_shp_file", type=str, default="_final_sample",
                        help="Suffix to be appended to the output shapefile name (default: _final_sample).")
    parser.add_argument("--output_folder", type=str, default="merged_samples_pine",
                        help="Name of the output folder for the merged shapefile. (default: merged_samples_pine)")

    args = parser.parse_args()

    merge_sqlites(args.dir_shapefile, args.gf_data_root, args.end_shp_file, args.output_folder)

