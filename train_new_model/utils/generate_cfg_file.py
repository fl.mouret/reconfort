import argparse
import os
import sys
from string import Template

def generate_cfg(label_year, S2_year, out_dir_cfg, out_dir_files, S2_path, ground_truth_path, list_tiles, number_of_chunks, v_model):

    with open(out_dir_cfg + 'config_Labels_' + label_year +'_year_' + S2_year + '.cfg', 'w') as f:
        d = {'label_year': label_year,
             'S2_year': S2_year,
             'out_dir_files': out_dir_files,
             'S2_path': S2_path,
             'ground_truth_path': ground_truth_path,
             'list_tiles': list_tiles,
             'number_of_chunks': number_of_chunks,
             'S2_start': S2_year + '0101', 'S2_end': S2_year + '1231'}  # dictionary with label year / S2 year
        s = Template("""chain :
        {
    output_path : '$out_dir_files/iota2_results_classif_labels-$label_year-S2_$S2_year/'
    remove_output_path : True
    check_inputs : True
    list_tile : 'T31UDQ T31UDP T31UCQ T31UCP T31TEN T31TEM T31TDN T31TDM T31TCN T31TCM T30TYT'
    data_field : 'dep_cor'
    s2_path : '$S2_path/$S2_year/'
    ground_truth : '$ground_truth_path'
    spatial_resolution : 10
    color_table : 'iota2/colorFile.txt'
    nomenclature_path : 'iota2/nomenclature.txt'
    first_step : 'init'
    last_step : 'sampling'
    proj : 'EPSG:2154'
}

Sentinel_2 :
{
    start_date : '$S2_start'
    end_date : '$S2_end'
}
                
arg_train :
{
    runs : 1
    features_from_raw_dates : False
    features : []
    classifier : 'sharkrf'
    otb_classifier_options : {'classifier.sharkrf.nodesize': 25}
    split_ground_truth : False
    sample_selection :
        {
            sampler : 'periodic'
            strategy : 'percent'
            'strategy.percent.p' : 1.0
            ratio : 1
        }
}

sensors_data_interpolation :
{
  auto_date : False
}
                
builders:
{
    builders_class_name: ["i2_classification"]
}

python_data_managing:
{
    chunk_size_mode:"split_number"
    number_of_chunks: $number_of_chunks
}

slurm:{
    account:'reconfort'
}
                
task_retry_limits:
{
    allowed_retry : 1
    maximum_ram : 120.0
    maximum_cpu : 60
}
        """)
        s = s.safe_substitute(d)
        f.write(s)


def main():
    parser = argparse.ArgumentParser(description="This function allow you to"
                                                 " generate a cfg file base on label year"
                                                 " and S2 data year"
                                     )

    parser.add_argument("-label_year",
                        dest="label_year",
                        help="Year of the labeled samples",
                        required=True)
    parser.add_argument("-S2_year",
                        dest="S2_year",
                        help="Year of the S2 images",
                        required=True)

    parser.add_argument("-out_dir",
                        dest="out_dir",
                        help="Out dir to save cfg file",
                        required=True)

    args = parser.parse_args()

    generate_cfg(args.label_year, args.S2_year, args.out_dir)


if __name__ == '__main__':
    sys.exit(main())
