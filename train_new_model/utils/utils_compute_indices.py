import numpy as np

def compute_TCW(dataframe):
    band_12 = [band for band in dataframe.columns.values if "_B12" in band]
    band_11 = [band for band in dataframe.columns.values if "_B11" in band]
    band_8 = [band for band in dataframe.columns.values if "_B8" in band and '_B8A' not in band]
    band_red = [band for band in dataframe.columns.values if "_B4" in band]
    band_green = [band for band in dataframe.columns.values if "_B3" in band]
    band_blue = [band for band in dataframe.columns.values if "_B2" in band]
    band_blue = [band for band in dataframe.columns.values if "_B2" in band]

    list_coefs = np.array([0.2578*dataframe[band_blue].values,
                           0.2305*dataframe[band_green].values,
                           0.0883*dataframe[band_red].values,
                           0.1071*dataframe[band_8].values,
                           -0.7611*dataframe[band_11].values,
                           -0.5308*dataframe[band_12].values])
    TCW = np.sum(list_coefs, axis=0)
    return TCW


def compute_TCW_13bands(dataframe):
    band_12 = [band for band in dataframe.columns.values if "_B12" in band]
    band_11 = [band for band in dataframe.columns.values if "_B11" in band]
    band_8 = [band for band in dataframe.columns.values if "_B8" in band and '_B8A' not in band]
    band_8A = [band for band in dataframe.columns.values if "_B8A" in band]
    band_7 = [band for band in dataframe.columns.values if "_B7" in band]
    band_6 = [band for band in dataframe.columns.values if "_B6" in band]
    band_5 = [band for band in dataframe.columns.values if "_B5" in band]
    band_red = [band for band in dataframe.columns.values if "_B4" in band]
    band_green = [band for band in dataframe.columns.values if "_B3" in band]
    band_blue = [band for band in dataframe.columns.values if "_B2" in band]

    list_coefs = np.array([0.1763*dataframe[band_blue].values,
                           0.1615*dataframe[band_green].values,
                           0.0486*dataframe[band_red].values,
                           0.0170 * dataframe[band_5].values,
                           0.0223 * dataframe[band_6].values,
                           0.0219 * dataframe[band_7].values,
                           0.1071*dataframe[band_8].values,
                           -0.0910 * dataframe[band_8A].values,
                           -0.7611*dataframe[band_11].values,
                           -0.5308*dataframe[band_12].values])
    TCW = np.sum(list_coefs, axis=0)
    return TCW


def compute_CRswir(dataframe):
    band_12 = [band for band in dataframe.columns.values if "_B12" in band]
    band_11 = [band for band in dataframe.columns.values if "_B11" in band]
    band_8a = [band for band in dataframe.columns.values if "_B8A" in band]

    # CRswir = dataframe[band_11].values / (
    #         1.1 + dataframe[band_8a].values + (1610 - 865) * (
    #             (dataframe[band_12].values - dataframe[band_8a].values) / (2190 - 865)))

    num = dataframe[band_11].values
    den = dataframe[band_8a].values + (1610 - 865) * (
                (dataframe[band_12].values - dataframe[band_8a].values) / (2190 - 865))
    den = np.where(den == 0, 1, den)

    CRswir = num / den
    return CRswir


def compute_EVI(dataframe):
    band_8 = [band for band in dataframe.columns.values if "_B8" in band and '_B8A' not in band]
    band_4 = [band for band in dataframe.columns.values if "_B4" in band]
    band_2 = [band for band in dataframe.columns.values if "_B2" in band]

    # CRswir = dataframe[band_11].values / (
    #         1.1 + dataframe[band_8a].values + (1610 - 865) * (
    #             (dataframe[band_12].values - dataframe[band_8a].values) / (2190 - 865)))

    num = 2.5*(dataframe[band_8].values - dataframe[band_4].values)
    den = dataframe[band_8].values + 6*dataframe[band_4].values - 7.5*dataframe[band_2].values + 1
    den = np.where(den == 0, 1, den)

    EVI = num / den
    return EVI


def compute_PSRI(dataframe):
    band_6 = [band for band in dataframe.columns.values if "_B6" in band]
    band_4 = [band for band in dataframe.columns.values if "_B4" in band]
    band_2 = [band for band in dataframe.columns.values if "_B2" in band]

    # CRswir = dataframe[band_11].values / (
    #         1.1 + dataframe[band_8a].values + (1610 - 865) * (
    #             (dataframe[band_12].values - dataframe[band_8a].values) / (2190 - 865)))

    num = (dataframe[band_4].values - dataframe[band_2].values)
    den = dataframe[band_6].values
    den = np.where(den == 0, 1, den)

    PSRI = num / den
    return PSRI


def compute_ratio_DES_vhvv(dataframe):
    band_DES_vh = [band for band in dataframe.columns.values if "_DES_vh" in band]
    band_DES_vv = [band for band in dataframe.columns.values if "_DES_vv" in band]

    num = dataframe[band_DES_vh].values
    den = dataframe[band_DES_vv].values + 1
    CRswir = num / den
    return CRswir


def compute_CRsum(dataframe):
    CRswir = compute_CRswir(dataframe)
    CRre = compute_CRre(dataframe)

    return CRswir + CRre


def compute_CCCI(dataframe):

    ndvi = compute_ndvi(dataframe)

    band_8 = [band for band in dataframe.columns.values if "_B8" in band and '_B8A' not in band]
    band_5 = [band for band in dataframe.columns.values if "_B5" in band]

    ND85 = (dataframe[band_8].values - dataframe[band_5].values) / (
            dataframe[band_8].values + dataframe[band_5].values)

    return ND85 / ndvi


def compute_CRre(dataframe):
    band_4 = [band for band in dataframe.columns.values if "_B4" in band]
    band_5 = [band for band in dataframe.columns.values if "_B5" in band]
    band_6 = [band for band in dataframe.columns.values if "_B6" in band]

    # CRre = dataframe[band_5].values / (
    #         dataframe[band_4].values + (704 - 665) * (
    #         (dataframe[band_6].values - dataframe[band_4].values) / (741 - 665)))

    num = dataframe[band_5].values
    den = dataframe[band_4].values + (704 - 665) * (
            (dataframe[band_6].values - dataframe[band_4].values) / (741 - 665))
    den = np.where(den == 0, 1, den)

    CRre = num / den

    return CRre


def compute_DRS(dataframe):
    band_12 = [band for band in dataframe.columns.values if "_B12" in band]
    band_4 = [band for band in dataframe.columns.values if "_B4" in band]

    DRS = np.sqrt((dataframe[band_4].values)**2 + (dataframe[band_12].values)**2)

    return DRS


def compute_CR_generic(dataframe, b1, b2, b3, lambda1, lambda2, lambda3):
    band_1 = [band for band in dataframe.columns.values if b1 in band]
    band_2 = [band for band in dataframe.columns.values if b2 in band]
    band_3 = [band for band in dataframe.columns.values if b3 in band]

    CR = dataframe[band_2].values / (
            0.1 + dataframe[band_1].values + (lambda2 - lambda1) * (
            (dataframe[band_3].values - dataframe[band_1].values) / (lambda3 - lambda1)))

    # if len(np.where(np.isinf(CR))[0]) !=0:
    #     print(np.where(np.isinf(CR)))
    #     idx = np.where(np.isinf(CR))[0]
    #     print(dataframe[band_1].values[idx])
    #     print(dataframe[band_2].values[idx])
    #     print(dataframe[band_3].values[idx])
    #     print(CR[idx])

    return CR


def compute_CR234(dataframe):

    CR234 = compute_CR_generic(dataframe, "_B2", "_B3", "_B4", 492.4, 559.8, 664.6)
    return CR234


def compute_CR345(dataframe):

    CR345 = compute_CR_generic(dataframe, "_B3", "_B4", "_B5", 559.8, 664.6, 704.1)
    return CR345


def compute_CR567(dataframe):

    CR345 = compute_CR_generic(dataframe, "_B5", "_B6", "_B7", 704.1, 740.5, 782.8)
    return CR345


def compute_REIP(dataframe):
    band_4 = [band for band in dataframe.columns.values if "_B4" in band]
    band_5 = [band for band in dataframe.columns.values if "_B5" in band]
    band_6 = [band for band in dataframe.columns.values if "_B6" in band]
    band_7 = [band for band in dataframe.columns.values if "_B7" in band]

    num = ((dataframe[band_4].values + dataframe[band_7].values) / 2) - dataframe[band_5].values
    den = 0.1 + (dataframe[band_6].values - dataframe[band_5].values)

    REIP = 705 + 35*(num/den)

    return REIP


def compute_ND56(dataframe):
    band_5 = [band for band in dataframe.columns.values if "_B5" in band]
    band_6 = [band for band in dataframe.columns.values if "_B6" in band]

    # ND56 = (dataframe[band_6].values - dataframe[band_5].values) / (
    #         dataframe[band_6].values + dataframe[band_5].values)

    num = dataframe[band_6].values - dataframe[band_5].values
    den = dataframe[band_6].values + dataframe[band_5].values
    den = np.where(den == 0, 1, den)
    ND56 = num/den

    return ND56


def compute_ND23(dataframe):
    band_5 = [band for band in dataframe.columns.values if "_B2" in band]
    band_6 = [band for band in dataframe.columns.values if "_B3" in band]

    # ND56 = (dataframe[band_6].values - dataframe[band_5].values) / (
    #         dataframe[band_6].values + dataframe[band_5].values)

    num = dataframe[band_6].values - dataframe[band_5].values
    den = dataframe[band_6].values + dataframe[band_5].values
    den = np.where(den == 0, 1, den)
    ND56 = num/den

    return ND56


def compute_ND78a(dataframe):
    band_7 = [band for band in dataframe.columns.values if "_B7" in band]
    band_8a = [band for band in dataframe.columns.values if "_B8A" in band]

    # ND56 = (dataframe[band_6].values - dataframe[band_5].values) / (
    #         dataframe[band_6].values + dataframe[band_5].values)

    num = dataframe[band_7].values - dataframe[band_8a].values
    den = dataframe[band_7].values + dataframe[band_8a].values
    den = np.where(den == 0, 1, den)
    ND56 = num/den

    return ND56


def compute_mNDVI705(dataframe):

    band_2 = [band for band in dataframe.columns.values if "_B2" in band]
    band_5 = [band for band in dataframe.columns.values if "_B5" in band]
    band_6 = [band for band in dataframe.columns.values if "_B6" in band]

    mNDVI705 = (dataframe[band_6].values - dataframe[band_5].values) / (
            dataframe[band_6].values + dataframe[band_5].values - 2*dataframe[band_2].values + 0.1)

    # if len(np.where(np.isinf(mNDVI705))[0]) !=0:
    #     print(np.where(np.isinf(mNDVI705)))
    #     idx = np.where(np.isinf(mNDVI705))[0]
    #     print(dataframe[band_6].values[idx])
    #     print(dataframe[band_5].values[idx])
    #     print(dataframe[band_2].values[idx])
    #     print(mNDVI705[idx])

    return mNDVI705


def compute_ND812(dataframe):
    band_8 = [band for band in dataframe.columns.values if "_B8" in band and '_B8A' not in band]
    band_12 = [band for band in dataframe.columns.values if "_B12" in band]

    ND812 = (dataframe[band_8].values - dataframe[band_12].values) / (
            dataframe[band_8].values + dataframe[band_12].values)

    return ND812


def compute_vari700(dataframe):
    band_5 = [band for band in dataframe.columns.values if "_B5" in band]
    band_red = [band for band in dataframe.columns.values if "_B4" in band]
    band_blue = [band for band in dataframe.columns.values if "_B2" in band]

    VARI = (dataframe[band_5].values - 1.7*dataframe[band_red].values + 0.7*dataframe[band_blue].values) / (1 +
            dataframe[band_5].values + 2.3*dataframe[band_red].values - 1.3*dataframe[band_blue].values)

    return VARI


def compute_ndvi(dataframe):
    band_8 = [band for band in dataframe.columns.values if "_B8" in band and '_B8A' not in band]
    band_red = [band for band in dataframe.columns.values if "_B4" in band]

    # NDVI = (dataframe[band_8].values - dataframe[band_red].values) / (
    #         0.1 + dataframe[band_8].values + dataframe[band_red].values)
    num = dataframe[band_8].values - dataframe[band_red].values
    den = dataframe[band_8].values + dataframe[band_red].values
    den = np.where(den == 0, 1, den)
    NDVI = num / den
    return NDVI


def compute_grvi(dataframe):
    band_green = [band for band in dataframe.columns.values if "_B3" in band]
    band_red = [band for band in dataframe.columns.values if "_B4" in band]

    GRVI = (dataframe[band_green].values - dataframe[band_red].values) / (1e-3 +
            dataframe[band_green].values + dataframe[band_red].values)
    return GRVI


def compute_ndwi_swir(dataframe):
    band_8 = [band for band in dataframe.columns.values if "_B8" in band and '_B8A' not in band]
    band_11 = [band for band in dataframe.columns.values if "_B11" in band]

    # NDWI = (dataframe[band_8].values - dataframe[band_11].values) / (
    #         0.1+ dataframe[band_8].values + dataframe[band_11].values)
    num = dataframe[band_8].values - dataframe[band_11].values
    den = dataframe[band_8].values + dataframe[band_11].values
    den = np.where(den == 0, 1, den)

    NDWI = num/den

    return NDWI


def compute_nd8A11(dataframe):
    band_8a = [band for band in dataframe.columns.values if "_B8A" in band]
    band_11 = [band for band in dataframe.columns.values if "_B11" in band]

    # NDWI = (dataframe[band_8].values - dataframe[band_11].values) / (
    #         0.1+ dataframe[band_8].values + dataframe[band_11].values)
    num = dataframe[band_8a].values - dataframe[band_11].values
    den = dataframe[band_8a].values + dataframe[band_11].values
    den = np.where(den == 0, 1, den)

    NDWI = num/den

    return NDWI


def compute_nd8A12(dataframe):
    band_8a = [band for band in dataframe.columns.values if "_B8A" in band]
    band_11 = [band for band in dataframe.columns.values if "_B12" in band]

    # NDWI = (dataframe[band_8].values - dataframe[band_11].values) / (
    #         0.1+ dataframe[band_8].values + dataframe[band_11].values)
    num = dataframe[band_8a].values - dataframe[band_11].values
    den = dataframe[band_8a].values + dataframe[band_11].values
    den = np.where(den == 0, 1, den)

    NDWI = num/den

    return NDWI


def compute_NG(dataframe):
    band_3 = [band for band in dataframe.columns.values if "_B3" in band]
    band_4 = [band for band in dataframe.columns.values if "_B4" in band]
    band_8a = [band for band in dataframe.columns.values if "_B8A" in band]

    NG = (dataframe[band_3].values) / (
            dataframe[band_8a].values + dataframe[band_4].values + dataframe[band_3].values)
    return NG


def compute_MCARI(dataframe):
    #MCARI = (5−4)−0.2(5−3)(5/4)
    #OSAVI = (1+0.16)*(8−4)/(8+4+0.16)

    band_3 = [band for band in dataframe.columns.values if "_B3" in band]
    band_4 = [band for band in dataframe.columns.values if "_B4" in band]
    band_5 = [band for band in dataframe.columns.values if "_B5" in band]
    band_8 = [band for band in dataframe.columns.values if "_B8" in band and '_B8A' not in band]

    MCARI = (dataframe[band_5].values - dataframe[band_4].values
             ) - 0.2*(dataframe[band_5].values - dataframe[band_3].values)*(dataframe[band_5].values/(dataframe[band_4].values+1))

    OSAVI = (1+0.16)*(dataframe[band_8].values - dataframe[band_4].values
                      )/(dataframe[band_8].values + dataframe[band_4].values + 0.16)

    return MCARI/OSAVI


def compute_ratio_B6B5(dataframe):
    band_5 = [band for band in dataframe.columns.values if "_B5" in band]
    band_6 = [band for band in dataframe.columns.values if "_B5" in band]

    return dataframe[band_6].values / (dataframe[band_5].values + 1)


def compute_CR_PCA(dataframe):
    CRre = compute_CRre(dataframe)
    CRswir = compute_CRswir(dataframe)

    CRpca = 0.6*CRre + 0.79*CRswir

    return CRpca