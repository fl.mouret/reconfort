import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
from scipy.signal import savgol_filter
from sklearn.experimental import enable_iterative_imputer  # noqa
from sklearn.impute import KNNImputer
import os
from scipy.interpolate import interp1d
from sklearn.preprocessing import MinMaxScaler


def gap_filling(data, list_names, verbose=0, method='linear'):
    '''
    :param X: feature matrix
    :param list_names: suppose that name is in format YYYYMMDD_INDICE
    :return:
    '''

    X = data.copy()
    non_observed_rows = np.where(~np.isnan(sum(X.T)) == False)[0]
    if len(non_observed_rows) == 0:
        return X

    if not isinstance(X, pd.DataFrame):
        X = pd.DataFrame(X, columns=list_names)

    list_feat = []
    list_date = []

    # find different features
    for name in list_names:
        feat_name = name[8::]
        date = name[0:8]

        if feat_name not in list_feat:
            list_feat.append(feat_name)

        if date not in list_date:
            list_date.append(date)

    dti = pd.to_datetime(pd.Series(list_date))
    for feat in list_feat:
        pd_buf = X[[col for col in X.columns if feat in col]]
        pd_buf = pd_buf.values
        # does not take time into account

        # take time into account
        pd_buf2 = pd.DataFrame(pd_buf, columns=dti)

        pd_buf2 = pd_buf2.interpolate(limit_direction="both", axis=1, method='slinear')

        if np.isnan(pd_buf2.values).any():
            pd_buf2 = pd_buf2.interpolate(limit_direction="both", axis=1, method='linear')

        X[[col for col in X.columns if feat in col]] = pd_buf2.values

    return X.values


def plot_median_IQR(ax, df, dti, color, label=''):
    ts_median, ts_Q1, ts_Q3 = compute_median_Q1Q3_ts(df, dti)
    ax.plot(ts_median, marker='.', linestyle='-', color=color)
    ax.fill_between(ts_median.index,
                    ts_Q1,
                    ts_Q3,
                    color=color,
                    label=label,
                    alpha=0.2)

    return ax

def plot_ts_3_classes(df_selectded, df_labels_selected, dti, title='', y_name='', save=False, outdir='', list_xticks=None, use_labels=False,
                      filter_xticks=True, legend=True):

    # fig, ax = plt.subplots(figsize=(5, 2), dpi=150)
    ts_median, ts_Q1, ts_Q3 = compute_median_Q1Q3_ts(df_selectded, dti)
    # ts_median_filtered, ts_Q1_filtered, ts_Q3_filtered = compute_median_Q1Q3_ts(df_filtered, dti)

    # ax.plot(ts_median, marker='.', linestyle='-', color='orange')
    # ax.fill_between(ts_median.index,
    #                 ts_Q1,
    #                 ts_Q3,
    #                 color='orange',
    #                 alpha=0.2)
    #
    # plt.show()

    if save:
        fig, ax = plt.subplots(figsize=(10, 4))
    else:
        fig, ax = plt.subplots(figsize=(5, 2), dpi=200)

    if not use_labels:
        df_sain = df_selectded[np.where(df_labels_selected < 0.2)[0], :]
        print('sain', df_sain.shape[0])
        ts_median_sain, ts_Q1_sain, ts_Q3_sain = compute_median_Q1Q3_ts(df_sain, dti)

        df_mid = df_selectded[np.where(np.logical_and(df_labels_selected >= 0.2,
                                                      df_labels_selected <= 0.5)
                                       )[0], :]
        ts_median_mid, ts_Q1_mid, ts_Q3_mid = compute_median_Q1Q3_ts(df_mid, dti)

        df_dead = df_selectded[np.where(df_labels_selected > 0.5)[0], :]
        ts_median_dead, ts_Q1_dead, ts_Q3_dead = compute_median_Q1Q3_ts(df_dead, dti)
    else:
        df_sain = df_selectded[np.where(df_labels_selected == 0)[0], :]
        print('sain', df_sain.shape[0])
        ts_median_sain, ts_Q1_sain, ts_Q3_sain = compute_median_Q1Q3_ts(df_sain, dti)

        df_mid = df_selectded[np.where(df_labels_selected == 1)[0], :]
        ts_median_mid, ts_Q1_mid, ts_Q3_mid = compute_median_Q1Q3_ts(df_mid, dti)

        df_dead = df_selectded[np.where(df_labels_selected == 2)[0], :]
        ts_median_dead, ts_Q1_dead, ts_Q3_dead = compute_median_Q1Q3_ts(df_dead, dti)

    ax.plot(ts_median_sain,
            # marker='.',
            linestyle='-', color='darkcyan',
            label='Healthy'
            # label='%D+<20% (#=' + str(df_sain.shape[0]) + ')'
            )
    ax.fill_between(ts_median.index,
                    ts_Q1_sain,
                    ts_Q3_sain,
                    color='darkcyan',
                    alpha=0.2)

    ax.plot(ts_median_mid,
            # marker='.',
            linestyle='-', color='orange',
            label='Declining'
            # label='20%=<%D+<50% (#=' + str(df_mid.shape[0]) + ')'
            )
    ax.fill_between(ts_median.index,
                    ts_Q1_mid,
                    ts_Q3_mid,
                    color='orange',
                    alpha=0.2)

    ax.plot(ts_median_dead,
            # marker='.',
            linestyle='-', color='red',
            label='Very declining'
            # label='50%=<%D+ (#=' + str(df_dead.shape[0]) + ')'
            )
    ax.fill_between(ts_median.index,
                    ts_Q1_dead,
                    ts_Q3_dead,
                    color='red',
                    alpha=0.2)

    ax.set_title(title)
    ax.set_ylabel(y_name)
    if legend:
        ax.legend()
    ax.grid(linestyle=':')

    if list_xticks is not None:
        ax.set_xticks(np.linspace(0, len(list_xticks) - 1, len(list_xticks)), labels=list_xticks, rotation=90)

    if filter_xticks:
        every_nth = 3
        for n, label in enumerate(ax.xaxis.get_ticklabels()):
            if n % every_nth != 0:
                label.set_visible(False)

    if save:
        if not os.path.exists(outdir):
            os.makedirs(outdir)

        print("save", outdir + title + y_name)
        plt.savefig(outdir + title + y_name + '.pdf', bbox_inches="tight")
        plt.close(fig)
    else:
        plt.show()


def get_stats_dataframe(df):
    list_year = np.unique(df['date'])
    df_array = df.values

    for year in list_year:
        print("YEAR", year)
        df_year = df.loc[np.where(df['date'] == year)[0], :]
        print('N placettes', len(np.unique(df_year["parcel_id_int"])))


def add_index_to_df(dataframe, dict_functions):
    list_dates = list(set([s[0:8] for s in dataframe.columns.values if '20' in s]))
    list_dates.sort()

    for date in list_dates:
        list_feat_date = [feature for feature in dataframe.columns.values if date in feature]
        df_date = dataframe[list_feat_date].copy()

        for name_fun in dict_functions:
            index = dict_functions[name_fun](df_date)
            dataframe[date + "_" + name_fun] = index
    return dataframe


def compute_median_Q1Q3_ts(df, dti):
    medians = np.nanmedian(df, axis=0)
    Q1 = np.nanpercentile(df, 10, axis=0)
    Q3 = np.nanpercentile(df, 90, axis=0)

    ts_median = pd.Series(medians, index=dti)
    ts_Q1 = pd.Series(Q1, index=dti)
    ts_Q3 = pd.Series(Q3, index=dti)

    return ts_median, ts_Q1, ts_Q3


def filter_ts_whittaker(dti, ts, d):
    w_cte = 1e-1
    # d = 3

    list_dti = dti.values
    t = [0]
    diffd = 0
    for dt in range(1, len(list_dti - 1)):
        date_0 = list_dti[dt - 1]
        date_1 = list_dti[dt]
        diffd += (date_1 - date_0) / np.timedelta64(1, 'D')
        t.append(diffd)
    t = np.asarray(t, dtype=int)

    w = np.ones(ts.shape)
    w[np.isnan(ts)] = w_cte
    x = ts
    x[np.isnan(ts)] = 0

    lambdas = [pow(10, m) for m in np.arange(0, 8)]
    # opt_lambda = whittakerSmootherOptLambda(t, x, w, lambdas, d=d)
    opt_lambda = 2
    ts_filtered = whittakerSmoother(t, x, w, l=opt_lambda, d=d)

    return ts_filtered


def filter_ts_savgol(dti, ts, window_length=4):
    list_dti = dti.values
    t = [0]
    diffd = 0
    for dt in range(1, len(list_dti - 1)):
        date_0 = list_dti[dt - 1]
        date_1 = list_dti[dt]
        diffd += (date_1 - date_0) / np.timedelta64(1, 'D')
        t.append(diffd)
    t = np.asarray(t, dtype=int)
    x = ts
    x[np.isnan(ts)] = 0
    ts_filtered = savgol_filter((t, x),
                                window_length=window_length, polyorder=1, deriv=0)

    return ts_filtered[1, :]


def convert_clouds_to_nan(dataframe, cloud_ratio=0.7):
    list_dates = list(set([s[0:8] for s in dataframe.columns.values if '20' in s]))
    list_dates.sort()

    list_dates_copy = list_dates.copy()

    for date in list_dates:

        # list_feat_date = [feature for feature in dataframe.columns.values if date in feature]
        # df_date = dataframe[list_feat_date].copy()
        # NG = compute_NG(df_date)
        # print(NG)
        # cond1 = NG > 0.15
        # cond2 = df_date[date + '_B2'] > 400
        # cond3 = df_date[date + '_B2'] > 700
        #
        # dataframe[date + '_MSK'].loc[np.where(np.logical_or(cond3, np.logical_and(cond1, cond2)))[0]] = 1
        # idx_filter = np.where(dataframe[date + '_B2'] > 600)[0]
        # dataframe[date + '_MSK'].loc[idx_filter] = 1

        idx_clouds = np.where(np.logical_or(dataframe[date + '_MSK'] != 0,
                                            np.logical_or(np.isnan(dataframe[date + '_B2']),
                                                          dataframe[date + '_B2'] > 600))
                              )[0]

        # Remove too cloudy images or unseen pixels...
        if len(idx_clouds) >= cloud_ratio * dataframe.shape[0]:
            print("too many clouds", len(idx_clouds), dataframe.shape[0])
            list_dates_copy.remove(date)
            list_feat_date = [feature for feature in dataframe.columns.values if date in feature]
            dataframe = dataframe.drop(columns=list_feat_date)
        else:
            list_feat_date = [feature for feature in dataframe.columns.values if date in feature]
            dataframe.loc[idx_clouds, list_feat_date] = np.nan

    return dataframe, list_dates_copy


def convert_undetected_clouds_to_nan(dataframe, cloud_ratio=0.7):
    list_dates = list(set([s[0:8] for s in dataframe.columns.values if '20' in s]))
    list_dates.sort()

    list_dates_copy = list_dates.copy()

    for date in list_dates:
        idx_clouds = np.where(dataframe[date + '_B2'] > 600)[0]

        # Remove too cloudy images or unseen pixels...
        if len(idx_clouds) >= cloud_ratio * dataframe.shape[0]:
            print("too many clouds", len(idx_clouds), dataframe.shape[0])
            list_dates_copy.remove(date)
            list_feat_date = [feature for feature in dataframe.columns.values if date in feature]
            dataframe = dataframe.drop(columns=list_feat_date)
        else:
            list_feat_date = [feature for feature in dataframe.columns.values if date in feature]
            dataframe.loc[idx_clouds, list_feat_date] = np.nan

    return dataframe, list_dates_copy


def smooth_ts(data, dti, downsampling=10, d=45, order=3, return_dti=False):
    np_selected = np.zeros(data.shape)

    list_dti = dti.values
    t = [0]
    diffd = 0
    for dt in range(1, len(list_dti - 1)):
        date_0 = list_dti[dt - 1]
        date_1 = list_dti[dt]
        diffd += (date_1 - date_0) / np.timedelta64(1, 'D')
        t.append(diffd)
    t = np.asarray(t, dtype=int)
    print(t)
    t_new = np.linspace(0, t[-1], int(t[-1] / downsampling))
    array_interpolated = np.zeros([data.shape[0], t_new.shape[0]])

    for i_ts in range(data.shape[0]):
        f = interp1d(x=t, y=data[i_ts, :], kind='linear', fill_value="extrapolate")
        ts_new = f(t_new)
        ts_new_filtered = savgol_filter(ts_new, int(d / downsampling), order)
        f2 = interp1d(x=t_new, y=ts_new_filtered, kind='linear', fill_value="extrapolate")
        np_selected[i_ts, :] = f2(t)
        array_interpolated[i_ts, :] = ts_new_filtered

    if return_dti:
        dti_regular = pd.to_datetime(t_new, unit='D', origin=dti[0])
        return np_selected, array_interpolated, dti_regular
    else:
        return np_selected, array_interpolated


def savitzky_golay_filtering(timeseries, wnds=[11, 7], orders=[2, 4], debug=True):
    interp_ts = pd.Series(timeseries)
    interp_ts = interp_ts.interpolate(method='linear', limit=14)
    smooth_ts = interp_ts
    wnd, order = wnds[0], orders[0]
    F = 1e8
    W = None
    it = 0
    while True:
        smoother_ts = savgol_filter(smooth_ts, window_length=wnd, polyorder=order)
        diff = smoother_ts - interp_ts
        sign = diff > 0
        if W is None:
            W = 1 - np.abs(diff) / np.max(np.abs(diff)) * sign
            wnd, order = wnds[1], orders[1]
        fitting_score = np.sum(np.abs(diff) * W)
        # print(it, ' : ', fitting_score)
        if fitting_score > F:
            break
        else:
            F = fitting_score
            it += 1
        smooth_ts = smoother_ts * sign + interp_ts * (1 - sign)
    if debug:
        return smooth_ts, interp_ts
    return smooth_ts


def generate_VI_TS(df, selected_band, only_summer=True, remove_clouds=True):
    list_columns_selected = []
    list_columns = df.columns

    if remove_clouds:
        df, list_dates = convert_clouds_to_nan(df, cloud_ratio=0.7)
    else:
        list_dates = list(set([s[0:8] for s in df.columns.values if '20' in s]))
        list_dates.sort()
        print(list_dates)
    dti = pd.to_datetime(pd.Series(list_dates))

    for col in list_columns:
        if selected_band in col:
            list_columns_selected.append(col)

    list_columns_selected.sort()
    df_selected = df[list_columns_selected]
    ##################################################################################################################
    # select months
    if only_summer:
        list_columns = df_selected.columns
        list_columns_selected = []
        selected_months = [
            # '05',
            '06',
            '07',
            '08',
            '09',
            # '10',
        ]
        for col in list_columns:
            month = col[4:6]
            for selected_month in selected_months:
                if selected_month == month:
                    list_columns_selected.append(col)

        list_columns_selected.sort()
        df_selected = df_selected[list_columns_selected]
        list_dates = list(set([s[0:8] for s in df_selected.columns.values if '20' in s]))
        list_dates.sort()
        dti = pd.to_datetime(pd.Series(list_dates))

    # ##################################################################################################################
    # ##################################################################################################################
    # # select years
    # list_columns = df_selected.columns
    # list_columns_selected = []
    # selected_years = ['2017', '2018', '2019', '2020', '2021']
    # for col in list_columns:
    #     year = col[0:4]
    #     for selected_year in selected_years:
    #         if selected_year == year:
    #             list_columns_selected.append(col)
    #
    # list_columns_selected.sort()
    # df_selected = df_selected[list_columns_selected]
    # list_dates = list(set([s[0:8] for s in df_selected.columns.values if '20' in s]))
    # list_dates.sort()
    # dti = pd.to_datetime(pd.Series(list_dates))
    #
    # ##################################################################################################################

    Norm = MinMaxScaler()

    print(df_selected.shape)

    # if selected_band == "CRswir":
    #     filtered_array = df_selected.values
    #
    #     filtered_array = np.where(filtered_array > 0.95, np.nan, filtered_array)
    #     filtered_array = np.where(df_selected < 0.7, np.nan, filtered_array)
    #
    #     df_selected = pd.DataFrame(filtered_array,
    #                                columns=df_selected.columns)
    # elif selected_band == "CRre":
    #     filtered_array = df_selected.values
    #
    #     filtered_array = np.where(filtered_array > 0.6, np.nan, filtered_array)
    #     filtered_array = np.where(df_selected < 0.35, np.nan, filtered_array)
    #
    #     df_selected = pd.DataFrame(filtered_array,
    #                                columns=df_selected.columns)
    #
    # elif selected_band == 'NDVI':
    #     filtered_array = df_selected.values
    #
    #     filtered_array = np.where(filtered_array > 0.99, np.nan, filtered_array)
    #     filtered_array = np.where(df_selected < 0.8, np.nan, filtered_array)
    #
    #     df_selected = pd.DataFrame(filtered_array,
    #                                columns=df_selected.columns)
    #
    # elif selected_band == 'ND56':
    #     filtered_array = df_selected.values
    #
    #     filtered_array = np.where(filtered_array > 0.9, np.nan, filtered_array)
    #     filtered_array = np.where(df_selected < 0.55, np.nan, filtered_array)
    #
    #     df_selected = pd.DataFrame(filtered_array,
    #                                columns=df_selected.columns)

    # filtered_array = df_selected.values
    #
    # idx_nans = np.isnan(filtered_array)
    # filtered_array = SimpleImputer().fit_transform(filtered_array)
    #
    # clf = IsolationForest(n_jobs=-1)
    # for col in range(filtered_array.shape[1]):
    #
    #     clf.fit(filtered_array[:, col].reshape(-1, 1))
    #     scores = clf.score_samples(filtered_array[:, col].reshape(-1,1))
    #     outliers = np.where(scores < -0.7)[0]
    #     filtered_array[outliers, col] = np.nan
    #     print(col, len(outliers))
    #
    # filtered_array[idx_nans] = np.nan
    # df_selected = pd.DataFrame(filtered_array,
    #                            columns=df_selected.columns)

    # print(df_selected)
    # print(df_selected.shape)
    # print(df_selected.values.shape)
    #
    # for col in range(df_selected.shape[1]):
    #     print(df_selected.values[:, col])
    #     print(len(np.where(np.isnan(df_selected.values[:, col]))[0]))

    # for col in range(df_selected.shape[1]):
    #     print(np.nanmax(df_selected.values[:, col]))
    df_selected_arr = df_selected.values
    df_selected_arr[np.where(np.isinf(df_selected))] = np.nan
    df_selected = pd.DataFrame(df_selected_arr, columns=df_selected.columns)
    df_selected = pd.DataFrame(Norm.inverse_transform(KNNImputer().fit_transform(Norm.fit_transform(df_selected))),
                               columns=df_selected.columns)
    # interpolate
    np_selected = df_selected.copy().values
    list_dti = dti.values

    list_diff = []
    t = [0]
    diffd = 0
    for dt in range(1, len(list_dti - 1)):
        date_0 = list_dti[dt - 1]
        date_1 = list_dti[dt]
        diffd += (date_1 - date_0) / np.timedelta64(1, 'D')
        list_diff.append((date_1 - date_0) / np.timedelta64(1, 'D'))
        t.append(diffd)
    t = np.asarray(t, dtype=int)
    print(len(t))
    print(list_diff)

    downsampling = 10
    t_new = np.linspace(0, t[-1], int(t[-1] / downsampling))
    print(len(t_new))

    array_interpolated = np.zeros([np_selected.shape[0], t_new.shape[0]])
    dti_regular = pd.to_datetime(t_new, unit='D', origin=dti[0])

    for i_ts in range(np_selected.shape[0]):
        f = interp1d(x=t, y=np_selected[i_ts, :], kind='linear')
        ts_new = f(t_new)
        ts_new_filtered = savitzky_golay_filtering(pd.Series(ts_new, index=dti_regular),
                                                   wnds=[int(120 / downsampling), int(90 / downsampling)],
                                                   orders=[2, 2],
                                                   debug=False)
        f2 = interp1d(x=t_new, y=ts_new_filtered, kind='linear')
        np_selected[i_ts, :] = f2(t)
        array_interpolated[i_ts, :] = ts_new_filtered


    df_selected_smooth = pd.DataFrame(np_selected, columns=df_selected.columns)

    return df_selected, df_selected_smooth


def convert_iota2_names(col):
    # print(col)
    if 'sentinel2' in col:
        name_col = col.split("_")
        date = name_col[2]
        band = name_col[1]
        band = band.replace('b', 'B')
        band = band.replace('mask', 'MSK')
        band = band.replace('8a', '8A')
        col = col.replace(col, date + '_' + band)
        # print(col)
    return col
