import os.path

import pandas as pd
import geopandas as gpd
import numpy as np
from iota2.common import otb_app_bank as otb
from shapely import wkt


def create_samples_file(data, list_col_for_iota2, labels_classif, samples_file):
    if not os.path.exists(samples_file):
        list_geom = []
        for i in range(data.shape[0]):
            list_geom.append("POINT (688962.072 6661314.902)")

        # list_geom = np.asarray(list_geom).reshape(-1,1)
        geom = pd.DataFrame(
            {'geom': list_geom})

        geom['geom'] = geom['geom'].apply(wkt.loads)
        list_col_OTB = list_col_for_iota2.copy()
        list_col_OTB.append('label')
        data = np.concatenate([data, labels_classif.reshape(-1, 1)], axis=1)

        gdp_temp = gpd.GeoDataFrame(data=data, columns=list_col_OTB, geometry=geom['geom'])
        gdp_temp.to_file(samples_file, driver='SQLite')


def train_shark_rf(data, labels_classif, samples_file, classifier_options, list_col_for_iota2, output_model='tmp_otb/model.txt'):
    list_col_for_iota2 = [x.lower() for x in list_col_for_iota2]
    # shape_id_pixel = gpd.read_file("HPC_features/" + v_merged_samples + "/geom_all_samples.gpkg")

    create_samples_file(data, list_col_for_iota2, labels_classif, samples_file)
    train_options = {
        "classifier": 'sharkrf',
        "in_vd": samples_file,
        "out_model": output_model,
        "data_field": 'label',
        "feats": list_col_for_iota2,
    }
    train_options["options"] = classifier_options

    app_train = otb.train_vector_classifier(**train_options)
    app_train.ExecuteAndWriteOutput()


def predict_shark_rf(data, output_model, samples_file, list_col_for_iota2, labels_classif):
    list_col_for_iota2 = [x.lower() for x in list_col_for_iota2]

    create_samples_file(data, list_col_for_iota2, labels_classif, samples_file)
    predict_options = {
        'model': output_model,
        'in': samples_file,
        'feat': list_col_for_iota2,
        'cfield': "predicted",
        'out': 'tmp_otb/classifiedData.shp',
        "confmap": True
    }
    app_pred = otb.create_vector_classifier_application(predict_options)
    app_pred.ExecuteAndWriteOutput()

    pred = gpd.read_file("tmp_otb/classifiedData.shp")

    return pred['predicted'].values, pred['confidence'].values